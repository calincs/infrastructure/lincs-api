FROM node:20-alpine AS base

# 1. Install dependencies only when needed
FROM base as deps
RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY package*.json .
COPY ./lincs-api-contracts ./lincs-api-contracts
RUN \
  if [ -f package-lock.json ]; then npm ci; \
  else echo "Lockfile not found." && exit 1; \
  fi

# 2. Rebuild the source code only when needed
FROM base AS builder
WORKDIR /app
COPY --from=deps /app/lincs-api-contracts ./lincs-api-contracts
COPY --from=deps /app/node_modules ./node_modules
COPY --from=deps /app/package*.json .
COPY ./src ./src
COPY ./tsconfig.json .
ENV NODE_ENV=production
ENV NEXT_TELEMETRY_DISABLED 1
RUN npm run build

# 3. Production image, copy all the files and run next
FROM base AS runner
WORKDIR /app
ENV NEXT_TELEMETRY_DISABLED 1
ENV NODE_ENV=production

COPY --from=deps /app/node_modules ./node_modules
COPY --from=deps /app/package.json .
COPY --from=deps /app/lincs-api-contracts ./lincs-api-contracts
COPY --from=builder --chown=node:node /app/dist ./dist
COPY --from=builder --chown=node:node /app/lincs-api-contracts/dist ./lincs-api-contracts/dist

USER node
EXPOSE 3000
ENV PORT 3000

CMD ["node", "dist/index.js"]