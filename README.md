# LINCS API

This service exposes secure functionality to authorized LINCS users and applications. It also includes some open endpoints for querying the LINCS knowledge graph. It also acts as a gateway to other LINCS services.

We use [Express](https://expressjs.com/) and [TS-REST](https://ts-rest.com/) as the backbone to build the server, combined with [keycloak-connect](https://github.com/keycloak/keycloak-nodejs-connect) to control access to resources.

The requests are validated in real-time using [Zod](https://zod.dev/). The server will respond immediately with the most appropriate status code and error message if the parameters, queries, or body post property are invalid for a specific endpoint.

## Contract as fully typed JavaScript helper library

This project is configured as a monorepo with a client library defined separately from the API to enable strongly typed API access. The `lincs-api-contracts` folder contains the contracts for this API. To use the API, simply import `@lincs.project/lincs-api-contracts` and follow the guide on the TS-REST website mentioned above. Alternatively, just use endpoints like a normal Json API by following the Open-API specs in the SwaggerUI.

Please look at the lincs-api-contracts [index.ts](./lincs-api-contracts/index.ts) to see what types and objects are exported.

## Development

It is strongly recommended to maintain this project with `VSCode` and use the official `ESLint` extension from _Microsoft_. The `Prettier - Code Formatter` from _prettier.io_ is also good to add.

### Environment

You need to create a local `.env` file that contain these variables:

```bash
KEYCLOAK_BASE_URL=https://keycloak.dev.lincsproject.ca
SESSION_SECRET=a_random_string
SPARQL_ENDPOINT="https://fuseki.lincsproject.ca/lincs"
SPARQL_PWD="admin_password"
SPACY_ENDPOINT="https://spacy.lincsproject.ca/entities"
PORT=3000
```

Only KEYCLOAK_BASE_URL is required. The default port is 3000.

### Logs

This project uses a [Winston](https://github.com/winstonjs/winston) logger to print logs to the console and [Morgan](https://github.com/expressjs/morgan) middleware to generate API logs automatically.

### Run with Docker

```bash
docker compose up
```

This should make the service accessible at `http://localhost:3000`

### Run locally with Node

If you want to host the API locally without using Docker, you must have Node.js v20+ and npm installed (instructions for how to download these dependencies can be found [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)).

To see if you already have Node.js and npm installed run the following commands:

```bash
npm version
```

To run the development server:

```node
npm install
npm run dev
```

Use `npm run clean` to delete build artifacts, `npm run test` to run unit tests, and `npm run lint` to run eslint checks.

### Creating new API endpoints

Create the endpoint's contract in `/contracts` and the implementation in `/routers`. Use the _readToken_ files for an example. Also add the new contract and router to their `index.ts` files in their folders. If the endpoint has complicated logic, the `services` folder can be used to implement helper methods. API docs will be automatically generated.
