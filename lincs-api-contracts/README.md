# LINCS-API Contracts

Defines the contract for the [LINCS API](https://gitlab.com/calincs/infrastructure/lincs-api). It uses [TS-REST](https://ts-rest.com/) in combination with [Zod](https://zod.dev/) to determine the request and response objects. These objects can be used to validate calls to each endpoint.

This contract package can be used to build a TypeScript helper to access the endpoints of the [LINCS API](https://gitlab.com/calincs/infrastructure/lincs-api) using a standardized fetcher. It allows for better endpoint discoverability and fully typed request parameters and responses. The API contract is also available to create a custom fetcher.

It must be used in conjunction with [`@ts-rest/core`](https://www.npmjs.com/package/@ts-rest/core)

## How to use the client helper

### Install

```text
npm install @lincs.project/lincs-api-contracts @ts-rest/core
```

### Create the client Adapter and use it

```js
import { contracts } from '@lincs.project/lincs-api-contracts';
import { initClient } from '@ts-rest/core';

const lincsAdapter = initClient(contracts, {
  baseUrl: 'https://lincs-api.lincsproject.ca',
  baseHeaders: {}
});

const response = await lincsAdapter.api.language.getLanguages();

if (response === 200) {
  console.log(response.body);
  // ["en", "fr"]
```

### Endpoints

Here is a list of the endpoints that can be accessed under `.api` with this contract client:

```text
annotations
dataset
entity
group
language
link
linked
links
ner
person
place
readToken
work
```

### Exported types

Here is a list of types exported by the package:

```text
Annotation
AnnotationsResponse
DigitalObject
EntityQuery
EntityResponse
Entity
FilteredEntityQuery
GroupResponse
LinksQuery
LinkMatch
LinkRequest
LinkResult
NerEntities
SimpleEntity
SimpleEntityQuery
SimpleStatement
SimpleStatementResponse
Statement
PersonResponse
PlaceResponse
Resource
TokenResponse
WorkResponse
```

## Development

This package is maintained in the [LINCS API repository](https://gitlab.com/calincs/infrastructure/lincs-api).
