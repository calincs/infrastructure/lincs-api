import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from "@anatine/zod-openapi";
import { zFilteredEntityQuery } from "./entity";

extendZodWithOpenApi(z);
const c = initContract();

export const zResource = z.object({
  uri: z.string().optional(),
  label: z.string().optional(),
});

export const zDigitalObject = zResource.extend({
  sources: z.array(zResource).optional(),
});

export const zDOAnnotationQuery = zFilteredEntityQuery.extend({
  uri: z.string().url()
    .openapi({ example: "http://viaf.org/viaf/30639106" }),
  doUri: z.string().url()
  .openapi({ example: "http://www.biographi.ca/en/bio/andre_alexis_12E.html" }),
  graphs: z.array(z.string()).optional()
    .openapi({ example: ["http://graph.lincsproject.ca/hist-canada/hist-cdns"] }),
});

export const zAnnotation = z.object({
  graph: z.string().optional(),
  annotation: z.string().optional(),
  annotationLabel: z.string().optional(),
  e55: z.string().optional(),
  info: z.string().optional(),
  infoLabel: z.string().optional(),
  quote: z.string().optional(),
  digitalObjects: z.array(zDigitalObject).optional(),
  resources: z.array(z.object({
    uri: z.string().optional(),
    labels: z.array(z.string()).optional(),
  })).optional(),
});

export const zAnnotationsResponse = z.array(zAnnotation);

export const annotationsContract = c.router({
  getAnnotations: {
    method: "POST",
    path: `/entityAnnotations`,
    body: zFilteredEntityQuery,
    responses: {
      200: zAnnotationsResponse.nullable(),
    },
    summary: "Returns Web Annotations for LINCS entities.",
  },
  getDOAnnotations: {
    method: "POST",
    path: `/entityAnnotations/do`,
    body: zDOAnnotationQuery,
    responses: {
      200: zAnnotationsResponse.nullable(),
    },
    summary: "Returns Web Annotations for the provided Digital Object and LINCS entity.",
    description: "This endpoint returns Web Annotations for a LINCS entity for which the provided Digital Object exists. The Digital Object is identified by the `doUri` parameter, and the LINCS entity is identified by the `uri` parameter. The `graphs` parameter is optional and can be used to filter the results by graph.",
  },
});
