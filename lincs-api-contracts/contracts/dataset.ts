import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from "@anatine/zod-openapi";
import { zSimpleEntityQuery } from "./entity";

extendZodWithOpenApi(z);
const c = initContract();

export const zSimpleStatement = z.object({
  predicate: z.string().optional(),
  predicateLabel: z.string().optional(),
  object: z.string().optional(),
  objectLabel: z.string().optional(),
  type: z.string().optional(),
  typeLabel: z.string().optional(),
});

export const zSimpleStatementResponse = z.array(zSimpleStatement);

export const zDatasetQuery = z.object({
  uri: z.string().openapi({ example : "http://graph.lincsproject.ca/orlando"}),
  language: z.string().default("en").optional().openapi({ example: "fr" }),
});

export const datasetContract = c.router({
  getDataset: {
    method: "POST",
    path: `/dataset`,
    body: zDatasetQuery,
    responses: {
      200: zSimpleStatementResponse.nullable(),
    },
    summary: "Returns outgoing statements for a LINCS dataset.",
    description: "Pass in the URI of the named graph that represents the dataset to get all recorded metadata for the dataset",
  },
});
