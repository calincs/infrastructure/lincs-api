import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from "@anatine/zod-openapi";

extendZodWithOpenApi(z);
const c = initContract();

export const zStatement = z.object({
  graph: z.string().optional(),
  predicate: z.string().optional(),
  predicateLabel: z.string().optional(),
  object: z.string().optional(),
  objectLabel: z.string().optional(),
  objectIsURI: z.boolean().optional(),
  type: z.string().optional(),
  typeLabel: z.string().optional(),
  startDate: z.date().optional(),
  endDate: z.date().optional(),
});

export const zSimpleEntity = z.object({
  resource: z.string(),
  resourceLabel: z.string().optional(),
  graph: z.string().optional(),
  type: z.string().optional(),
  typeLabel: z.string().optional(),
  startDate: z.date().optional(),
  endDate: z.date().optional(),
  outgoing: z.array(zStatement).optional(),
});

export const zEntity = zSimpleEntity.extend({
  incoming: z.array(zSimpleEntity).optional(),
});

export const zEntityResponse = z.array(zEntity);

export const zSimpleEntityQuery = z.object({
  uris: z.array(z.string().url()).nonempty()
    .openapi({ example: ["http://viaf.org/viaf/39385478"] }),
  language: z.string().default("en").optional().openapi({ example: "fr" }),
});

export const zFilteredEntityQuery = z.object({
  uri: z.string().url()
    .openapi({ example: "http://viaf.org/viaf/39385478" }),
  language: z.string().default("en").optional().openapi({ example: "fr" }),
  graphs: z.array(z.string()).optional()
    .openapi({ example: ["http://graph.lincsproject.ca/orlando"] }),
  page: z.number().default(1).openapi({ example: "1" }),
});

export const zEntityQuery = zSimpleEntityQuery.extend({
  graphs: z.array(z.string()).optional()
    .openapi({ example: ["http://graph.lincsproject.ca/orlando"] }),
  page: z.number().default(1).openapi({ example: "1" }),
});

export const entityContract = c.router({
  getEntity: {
    method: "POST",
    path: `/entity`,
    body: zEntityQuery,
    responses: {
      200: zEntityResponse.nullable(),
    },
    summary: "Returns outgoing statements for LINCS entities.",
    description:
      "The statements are returned as a JSON array of triples that conforms to the Entity schema object below. If the graphs property is omitted, the service will not filter to graph and return triples found in all graphs.",
  },
  getIncoming: {
    method: "POST",
    path: `/entity/incoming`,
    body: zFilteredEntityQuery,
    responses: {
      200: zEntityResponse.nullable(),
    },
    summary: "Returns incoming statements for LINCS entities.",
    description:
      "The statements are returned as a JSON array of triples that conforms to the Entity schema object below. If the graphs property is omitted, the service will not filter to graph and return triples found in all graphs.",
  },
  findEntities: {
    method: "POST",
    path: `/entity/exists`,
    body: z.array(z.string()).openapi({ example: ["http://viaf.org/viaf/39385478", "http://does_not_exist"] }),
    responses: {
      200: z.array(z.string()),
    },
    summary: "Returns a list of entities found in the LINCS triplestore.",
    description:
      "This endpoint will check each URI in the submitted list if it exists in the LINCS triplestore. It will then return a list of URIs found.",
  },
});
