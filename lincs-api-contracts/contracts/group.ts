import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from '@anatine/zod-openapi';
import { zResource } from "./annotations";
import { zSimpleEntityQuery } from "./entity";
import { zEntityStatements } from "./person";

extendZodWithOpenApi(z);
const c = initContract();

export const zGroup = z.object({
  uri: z.string().url(),
  graph: z.string().optional(),
  label: z.string().optional(),
  types: z.array(zResource).optional(),
  members: z.array(zResource).optional(),
  works: z.array(zResource).optional(),
});

const zGroupQuery = zSimpleEntityQuery.extend({
  uris: z.array(z.string().url()).nonempty().openapi({ example: ["http://viaf.org/viaf/126044314"] }),
});

export const groupContract = c.router({
  getGroup: {
    method: "POST",
    path: `/group`,
    body: zGroupQuery,
    responses: {
      200: z.array(zGroup).nullable(),
    },
    summary: "Summary of a group entity in the LINCS knowledge graph.",
  },
  getGroupStatements: {
    method: "POST",
    path: `/group/statements`,
    body: zGroupQuery,
    responses: {
      200: z.array(zEntityStatements).nullable(),
    },
    summary: "A group entity with statements from the LINCS knowledge graph.",
  },
});
