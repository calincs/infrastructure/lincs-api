import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from "@anatine/zod-openapi";

extendZodWithOpenApi(z);
const c = initContract();

export const languageContract = c.router({
  getLanguages: {
    method: "GET",
    path: `/language`,
    responses: {
      200: z.array(z.string()).nullable(),
    },
    summary: "Returns a list of supported languages for NER in ISO2 format.",
  },
  getLanguage: {
    method: "POST",
    path: `/language`,
    body: z.object({ text: z.string().openapi({ example: "This is an English sentence." }) }),
    responses: {
      200: z.object({ language: z.string().nullable() }),
    },
    summary: "Returns the most likely language in ISO2 format of the string passed in. ",
  },
});
