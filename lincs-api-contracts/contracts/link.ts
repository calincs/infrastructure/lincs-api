import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from '@anatine/zod-openapi';

extendZodWithOpenApi(z);
const c = initContract();

export enum Authorities {
  DBpedia_All = "DBpedia-All",
  DBpedia_Event = "DBpedia-Event",
  DBpedia_Organisation = "DBpedia-Organisation",
  DBpedia_Person = "DBpedia-Person",
  DBpedia_Place = "DBpedia-Place",
  DBpedia_Work = "DBpedia-Work",
  GeoNames = "Geonames",
  Getty_ALL = "Getty-All",
  Getty_AAT = "Getty-AAT",
  Getty_CONA = "Getty-CONA",
  Getty_TGN = "Getty-TGN",
  Getty_ULAN = "Getty-ULAN",
  GND_Organisation = "GND-Organisation",
  GND_Person = "GND-Person",
  GND_Place = "GND-Place",
  GND_Subject = "GND-Subject",
  GND_Work = "GND-Work",
  LINCS_ALL = "LINCS-All",
  LINCS_Person = "LINCS-Person",
  LINCS_Place = "LINCS-Place",
  LINCS_Work = "LINCS-Work",
  LINCS_Group = "LINCS-Group",
  LINCS_Event = "LINCS-Event",
  VIAF_Bibliographic = "VIAF-Bibliographic",
  VIAF_Corporate = "VIAF-Corporate",
  VIAF_Expressions = "VIAF-Expressions",
  VIAF_Geographic = "VIAF-Geographic",
  VIAF_Personal = "VIAF-Personal",
  VIAF_Works = "VIAF-Works",
  Wikidata = "Wikidata",
}
export type Authority = `${Authorities}`; // => type Authority = "VIAF" | "Getty"

export const zAuthorities = z.array(z.string());

export const zLinkMatch = z.object({
  uri: z.string(),
  label: z.string(),
  description: z.string().optional(),
});

export const zLinkResult = z.object({
  authority: z.string(),
  matches: z.array(zLinkMatch).optional(),
});

export const zLinkRequest = z.object({
  entity: z.string().openapi({ example: "Virginia Woolf" }),
  authorities: z.array(z.string()).default(["LINCS"]).optional().openapi({ example: ["VIAF-Personal", "Wikidata"]}),
  moreResults: z.boolean().optional().openapi({ example: false }),
});

export const linkContract = c.router({
  getAuthorities: {
    method: "GET",
    path: `/link/authority`,
    responses: {
      200: zAuthorities.nullable(),
    },
    summary: "A list of supported Authorities for reconciliation.",
  },
  reconcile: {
    method: "POST",
    path: `/link/reconcile`,
    body: zLinkRequest,
    responses: {
      200: z.array(zLinkResult).nullable(),
    },
    summary: "Returns possible matches for a given entity and authorities.",
  },
});
