import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from "@anatine/zod-openapi";
import { zEntityResponse, zFilteredEntityQuery} from "./entity";

extendZodWithOpenApi(z);
const c = initContract();

export const linkedContract = c.router({
  getLinked: {
    method: "POST",
    path: `/linked`,
    body: zFilteredEntityQuery,
    responses: {
      200: zEntityResponse.nullable(),
    },
    summary: "Returns entities that are connected to the given entity via events.",
    description:
      "The entities are returned as a JSON array of objects that conforms to the schema below. If the graphs property is omitted, the service will not filter to graph and return triples found in all graphs.",
  },
});
