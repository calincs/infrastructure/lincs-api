import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from "@anatine/zod-openapi";
import { zEntityResponse, zFilteredEntityQuery } from "./entity";

extendZodWithOpenApi(z);
const c = initContract();

export const zLinksQuery = zFilteredEntityQuery.extend({
  secondUri: z.string().openapi({ example: "http://www.wikidata.org/entity/Q703935" }),
  predicateFilter: z.array(z.string()).optional().openapi({
    example: [
      "http://www.cidoc-crm.org/cidoc-crm/P107_has_current_or_former_member",
      "http://www.cidoc-crm.org/cidoc-crm/P67_refers_to"
    ]
  }),
});

export const linksContract = c.router({
  getLinks: {
    method: "POST",
    path: `/links`,
    body: zLinksQuery,
    responses: {
      200: zEntityResponse.nullable(),
    },
    summary: "Returns events that connect two entities.",
    description:
      "The statements are returned as a JSON array of triples that conforms to the Entity schema object below. If the graphs property is omitted, the service will not filter to graph and return triples found in all graphs.",
  },
});
