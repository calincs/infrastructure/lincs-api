import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from '@anatine/zod-openapi';

extendZodWithOpenApi(z);
const c = initContract();

const zMessage = z.object({
  message: z.string().optional(),
});

const zNerMatch = z.object({
  start: z.number(),
  end: z.number(),
  text: z.string(),
});

const zNerEntity = z.object({
  name: z.string(),
  label: z.string().optional(),
  matches: z.array(zNerMatch).optional(),
});

export const zNerEntities = z.object({
  entities: z.array(zNerEntity).optional(),
  errors: z.array(zMessage).optional(),
  warnings: z.array(zMessage).optional(),
});

const zNerValidationError = z.object({
  loc: z.array(z.any()).optional(),
  msg: z.string().optional(),
  type: z.string().optional(),
});

const zNerQuery = z.object({
  text: z.string().openapi({ example: "But Google is starting from behind. The company made a late push into hardware, and Apple's Siri, available on iPhones, and Amazon's Alexa software, which runs on its Echo and Dot devices, have clear leads in consumer adoption." }),
  language: z.string().default("en").optional().openapi({ example: "en" }),
});

export const nerContract = c.router({
  getEntities: {
    method: "POST",
    path: `/ner`,
    body: zNerQuery,
    responses: {
      200: zNerEntities.nullable(),
      422: zNerValidationError.nullable(),
    },
    summary: "NER on given text that returns a list of entities with offsets.",
  },
});
