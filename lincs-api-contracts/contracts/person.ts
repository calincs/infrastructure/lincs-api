import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from '@anatine/zod-openapi';
import { zResource } from "./annotations";
import { zSimpleEntityQuery } from "./entity";

extendZodWithOpenApi(z);
const c = initContract();

export const zEntityStatement = z.object({
  type: z.string(),
  graph: z.string().optional(),
  uri: z.string().url().optional(),
  value: z.string().optional(),
});

export const zEntityStatements = z.object({
  uri: z.string().url(),
  statements: z.array(zEntityStatement),
});

export const zPerson = z.object({
  uri: z.string().url(),
  graph: z.string().optional(),
  label: z.string().optional(),
  prefLabel: z.string().optional(),
  image: z.string().optional(),
  note: z.string().optional(),
  birthDate: z.string().optional(),
  birthPlace: z.string().optional(),
  deathDate: z.string().optional(),
  deathPlace: z.string().optional(),
  sameAs: z.array(z.string()).optional(),
  pursuits: z.array(zResource).optional(),
  attributes: z.array(zResource).optional(),
  works: z.array(zResource).optional(),
  groups: z.array(zResource).optional(),
});

export const personContract = c.router({
  getPerson: {
    method: "POST",
    path: `/person`,
    body: zSimpleEntityQuery,
    responses: {
      200: z.array(zPerson).nullable(),
    },
    summary: "Summary of a person entity in the LINCS knowledge graph.",
  },
  getPersonStatements: {
    method: "POST",
    path: `/person/statements`,
    body: zSimpleEntityQuery,
    responses: {
      200: z.array(zEntityStatements).nullable(),
    },
    summary: "A person entity with statements from the LINCS knowledge graph.",
  },
});
