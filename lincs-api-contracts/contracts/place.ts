import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from '@anatine/zod-openapi';
import { zResource } from "./annotations";
import { zSimpleEntityQuery } from "./entity";
import { zEntityStatements } from "./person";

extendZodWithOpenApi(z);
const c = initContract();

export const zPlace = z.object({
  uri: z.string().url(),
  graph: z.string().optional(),
  label: z.string().optional(),
  labels: z.array(zResource).optional(),
  featureClass: z.string().optional(),
  definedBy: z.string().optional(),
  types: z.array(zResource).optional(),
  bornHere: z.array(zResource).optional(),
  diedHere: z.array(zResource).optional(),
});

const zPlaceQuery = zSimpleEntityQuery.extend({
  uris: z.array(z.string().url()).nonempty().openapi({ example: ["https://sws.geonames.org/2633352"] }),
});

export const placeContract = c.router({
  getPlace: {
    method: "POST",
    path: `/place`,
    body: zPlaceQuery,
    responses: {
      200: z.array(zPlace).nullable(),
    },
    summary: "Summary of a place entity in the LINCS knowledge graph.",
  },
  getPlaceStatements: {
    method: "POST",
    path: `/place/statements`,
    body: zPlaceQuery,
    responses: {
      200: z.array(zEntityStatements).nullable(),
    },
    summary: "A place entity with statements from the LINCS knowledge graph.",
  },
});
