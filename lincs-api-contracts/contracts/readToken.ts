import { initContract } from "@ts-rest/core";
import { z } from "zod";

const c = initContract();

export const zTokenResponse = z.object({
  clientId: z.string(),
  userSession: z.string(),
  username: z.string(),
  email: z.string(),
  realmRoles: z.array(z.string()),
});

export const readTokenContract = c.router({
  getToken: {
    method: "GET",
    path: `/readToken`,
    responses: {
      200: zTokenResponse.nullable(),
    },
    summary: "Decodes the Authorization Token and returns Json",
  },
});
