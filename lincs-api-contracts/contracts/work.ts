import { initContract } from "@ts-rest/core";
import { z } from "zod";
import { extendZodWithOpenApi } from '@anatine/zod-openapi';
import { zResource } from "./annotations";
import { zSimpleEntityQuery } from "./entity";
import { zEntityStatements } from "./person";

extendZodWithOpenApi(z);
const c = initContract();

export const zWork = z.object({
  uri: z.string().url(),
  graph: z.string().optional(),
  label: z.string().optional(),
  createDate: z.string().optional(),
  types: z.array(zResource).optional(),
  creators: z.array(zResource).optional(),
});

const zWorkQuery = zSimpleEntityQuery.extend({
  uris: z.array(z.string().url()).nonempty()
    .openapi({ example: ["https://commons.cwrc.ca/orlando:bd78cc51-d063-4c4f-ae3c-10d8cc5c85f9"] }),
});

export const workContract = c.router({
  getWork: {
    method: "POST",
    path: `/work`,
    body: zWorkQuery,
    responses: {
      200: z.array(zWork).nullable(),
    },
    summary: "Summary of a work entity in the LINCS knowledge graph.",
  },
  getWorkStatements: {
    method: "POST",
    path: `/work/statements`,
    body: zWorkQuery,
    responses: {
      200: z.array(zEntityStatements).nullable(),
    },
    summary: "A work entity with statements from the LINCS knowledge graph.",
  },
});
