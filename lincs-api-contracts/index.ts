import { initContract } from "@ts-rest/core";
import { z } from "zod";
import {
  entityContract,
  zEntityResponse,
  zEntityQuery,
  zEntity,
  zSimpleEntity,
  zSimpleEntityQuery,
  zStatement,
  zFilteredEntityQuery,
} from "./contracts/entity";
import { Authorities, linkContract, zLinkMatch, zLinkRequest, zLinkResult } from "./contracts/link";
import { linkedContract } from "./contracts/linked";
import { linksContract, zLinksQuery } from "./contracts/links";
import { nerContract, zNerEntities } from "./contracts/ner";
import { personContract, zPerson, zEntityStatement, zEntityStatements } from "./contracts/person";
import { placeContract, zPlace } from "./contracts/place";
import { readTokenContract, zTokenResponse } from "./contracts/readToken";
import { annotationsContract, zAnnotation, zAnnotationsResponse, zDigitalObject, zDOAnnotationQuery, zResource } from "./contracts/annotations";
import { datasetContract, zDatasetQuery, zSimpleStatement, zSimpleStatementResponse } from "./contracts/dataset";
import { languageContract } from "./contracts/language";
import { workContract, zWork } from "./contracts/work";
import { groupContract, zGroup } from "./contracts/group";

export const AUTHORITIES = Authorities;

export type Annotation = z.infer<typeof zAnnotation>;
export type AnnotationsResponse = z.infer<typeof zAnnotationsResponse>;
export type DatasetQuery = z.infer<typeof zDatasetQuery>;
export type DigitalObject = z.infer<typeof zDigitalObject>;
export type DOAnnotationQuery = z.infer<typeof zDOAnnotationQuery>;
export type Entity = z.infer<typeof zEntity>;
export type EntityQuery = z.infer<typeof zEntityQuery>;
export type EntityResponse = z.infer<typeof zEntityResponse>;
export type EntityStatement = z.infer<typeof zEntityStatement>;
export type EntityStatements = z.infer<typeof zEntityStatements>;
export type FilteredEntityQuery = z.infer<typeof zFilteredEntityQuery>;
export type Group = z.infer<typeof zGroup>;
export type LinksQuery = z.infer<typeof zLinksQuery>;
export type LinkMatch = z.infer<typeof zLinkMatch>;
export type LinkRequest = z.infer<typeof zLinkRequest>;
export type LinkResult = z.infer<typeof zLinkResult>;
export type NerEntities = z.infer<typeof zNerEntities>;
export type SimpleEntity = z.infer<typeof zSimpleEntity>;
export type SimpleEntityQuery = z.infer<typeof zSimpleEntityQuery>;
export type SimpleStatement = z.infer<typeof zSimpleStatement>;
export type SimpleStatementResponse = z.infer<typeof zSimpleStatementResponse>;
export type Statement = z.infer<typeof zStatement>;
export type Person = z.infer<typeof zPerson>;
export type Place = z.infer<typeof zPlace>;
export type Resource = z.infer<typeof zResource>;
export type TokenResponse = z.infer<typeof zTokenResponse>;
export type Work = z.infer<typeof zWork>;

const c = initContract();
export const contracts = c.router({
  api: c.router(
    {
      annotations: annotationsContract,
      dataset: datasetContract,
      entity: entityContract,
      group: groupContract,
      language: languageContract,
      link: linkContract,
      linked: linkedContract,
      links: linksContract,
      ner: nerContract,
      person: personContract,
      place: placeContract,
      readToken: readTokenContract,
      work: workContract,
    },
    { pathPrefix: "/api" }
  ),
});
