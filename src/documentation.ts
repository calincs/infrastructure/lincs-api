import { generateOpenApi } from "@ts-rest/open-api";
import pkg from "../package.json" assert { type: "json" };
import { contracts } from "@lincs.project/lincs-api-contracts";
import { logger } from "@/helpers/logger";

//@ts-expect-error no ts defs for OpenAPIObject
let swaggerDoc;

export const getDocument = (serverUrl: string) => {
  //@ts-expect-error no ts defs for OpenAPIObject
  if (!swaggerDoc) {
    logger.info(`Swagger UI generated at ${serverUrl}/open-api`);
    swaggerDoc = generateOpenApi(
      contracts.api,
      {
        info: {
          title: "LINCS API",
          description: `Source: [${serverUrl}/open-api](${serverUrl}/open-api)`,
          version: pkg.version,
        },
        servers: [{ url: serverUrl }],
        security: [{ bearerAuth: [] }],
        components: {
          securitySchemes: {
            bearerAuth: {
              type: "http",
              scheme: "bearer",
              bearerFormat: "JWT",
            },
          },
        },
        externalDocs: {
          description: "Project Repository",
          url: "https://gitlab.com/calincs/infrastructure/lincs-api",
        },
      },
      {
        jsonQuery: true,
        setOperationId: true,
      }
    );
  }
  return swaggerDoc;
};
