import "dotenv/config";

export const PORT = process.env.PORT ?? 3000;
export const KEYCLOAK_BASE_URL = process.env.KEYCLOAK_BASE_URL ?? "https://keycloak.dev.lincsproject.ca";
export const SESSION_SECRET = process.env.SESSION_SECRET ?? "";
export const SPARQL_ENDPOINT = process.env.SPARQL_ENDPOINT ?? "https://fuseki.lincsproject.ca/lincs";
export const SPARQL_PWD = process.env.SPARQL_PWD ?? "";
export const SPACY_ENDPOINT = process.env.SPACY_ENDPOINT ?? "https://spacy.lincsproject.ca/entities";
export const GEONAMES_ENDPOINT = process.env.GEONAMES_ENDPOINT ?? "https://geonames.lincsproject.ca/geocode";
export const PUBLIC_HOSTNAME = KEYCLOAK_BASE_URL.replace("keycloak", "lincs-api").replace("https://", "");