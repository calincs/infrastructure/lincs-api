interface ErrorWithMessage {
  message: string;
}

/**
 * The function `isErrorWithMessage` checks if an object is an `ErrorWithMessage` type by verifying if
 * it has a `message` property of type string.
 * @param {unknown} error - The `error` parameter is of type `unknown`, which means it can be any type.
 * @returns The function `isErrorWithMessage` is returning a boolean value.
 */
const isErrorWithMessage = (error: unknown): error is ErrorWithMessage => {
  return (
    typeof error === 'object' &&
    error !== null &&
    'message' in error &&
    typeof (error as Record<string, unknown>).message === 'string'
  );
}

/**
 * The function `toErrorWithMessage` converts an unknown value into an `ErrorWithMessage` object,
 * either by returning the value if it is already an `ErrorWithMessage`, or by creating a new `Error`
 * object with a JSON string representation of the value.
 * @param {unknown} maybeError - The `maybeError` parameter is of type `unknown`, which means it can be
 * any type. It represents a value that may or may not be an `ErrorWithMessage` object.
 * @returns The function `toErrorWithMessage` returns an instance of `ErrorWithMessage`.
 */
const toErrorWithMessage = (maybeError: unknown): ErrorWithMessage => {
  if (isErrorWithMessage(maybeError)) return maybeError;

  try {
    return new Error(JSON.stringify(maybeError));
  } catch {
    // fallback in case there's an error stringifying the maybeError
    // like with circular references for example.
    return new Error(String(maybeError));
  }
}

/**
 * The function `getErrorMessage` takes an error object and returns its error message.
 * @param {unknown} error - The `error` parameter is of type `unknown`, which means it can be any type of value.
 * @returns the message property of the error object after converting it to an error object with a
 * message using the `toErrorWithMessage` function.
 */
export const getErrorMessage = (error: unknown) => {
  return toErrorWithMessage(error).message;
}
