import Keycloak from 'keycloak-connect';
import session from 'express-session';
import { logger } from '@/helpers/logger';
import { KEYCLOAK_BASE_URL, SESSION_SECRET } from '@/globals';

const memoryStore = new session.MemoryStore();

//*  According to https://www.keycloak.org/docs/latest/securing_apps/#_nodejs_adapter
export const Session = session({
  resave: false,
  saveUninitialized: true,
  secret: SESSION_SECRET,
  store: memoryStore,
});

// According to https://www.keycloak.org/docs/latest/securing_apps/#_nodejs_adapter
const keycloakConfig: Keycloak.KeycloakConfig = {
  realm: 'lincs',
  'auth-server-url': KEYCLOAK_BASE_URL,
  'ssl-required': 'external',
  resource: 'admin-cli',
  'bearer-only': true,
  'confidential-port': 0,
}

export let _keycloak!: Keycloak.Keycloak;

export const initKeycloak = () => {
  if (_keycloak) return _keycloak;

  logger.info('Initializing Keycloak...');
  _keycloak = new Keycloak({ store: memoryStore }, keycloakConfig);
  return _keycloak;
}

export const KC = initKeycloak();
