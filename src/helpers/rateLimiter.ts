import { RateLimiterMemory } from "rate-limiter-flexible";
import { Request, Response, NextFunction } from 'express';
import jsonwebtoken from "jsonwebtoken";
import { logger } from "@/helpers/logger";
import { getErrorMessage } from "@/helpers/error";
import { KEYCLOAK_BASE_URL } from "@/globals";
import { PUBLIC_HOSTNAME } from "@/globals";

// different examples here: https://github.com/animir/node-rate-limiter-flexible/wiki/Overall-example

const opts = {
  points: 500, // consumable points
  duration: 30, // per number of seconds
};
const DEFAULT_POINTS_PER_REQUEST = 50;
const PENALTY_POINTS_PER_REQUEST = 500;
const ADMIN_POINTS_PER_REQUEST = 1;
const USER_POINTS_PER_REQUEST = 2;
const LINCS_POINTS_PER_REQUEST = 5;

const rateLimiter = new RateLimiterMemory(opts);

export const rateLimiterMiddleware = (req: Request, res: Response, next: NextFunction) => {
  let key = req.ip ?? "no_ip";
  let pointsToConsume = DEFAULT_POINTS_PER_REQUEST;
  if (req.headers.host === PUBLIC_HOSTNAME) // lincs-api.lincsproject.ca
    pointsToConsume = LINCS_POINTS_PER_REQUEST;
  try {
    let authToken = req.headers.authorization ?? "nothing";
    if (authToken.substring(0, 7) === "Bearer ")
      authToken = authToken.substring(7);
    if (authToken !== "nothing") {
      const token = jsonwebtoken.decode(authToken);
      // console.log(token);
      if (!token || typeof token === "string") {
        pointsToConsume = PENALTY_POINTS_PER_REQUEST; // bad auth token
      } else {
        // token decode successful. Validate the token first
        const issuer = KEYCLOAK_BASE_URL + "/realms/lincs";
        if (issuer !== token.iss) {
          pointsToConsume = PENALTY_POINTS_PER_REQUEST;
        } else {
          // Check token expiration
          const currentTime = Math.floor(Date.now() / 1000); // Current timestamp in seconds
          if (!token.exp || token.exp < currentTime) {
            pointsToConsume = DEFAULT_POINTS_PER_REQUEST;
          } else {
            // Determine points to consume
            key = token.preferred_username;
            const clientId = token.azp; // "nerve"
            const realmRoles = token.realm_access.roles; // ["Admin", "User", "default_roles_lincs"]
            if (realmRoles && ("nerve" === clientId || "leaf-writer" === clientId || "lincs-api" === clientId)) {
              if (realmRoles.includes("Admin")) {
                pointsToConsume = ADMIN_POINTS_PER_REQUEST;
              } else if (realmRoles.includes("User")) {
                pointsToConsume = USER_POINTS_PER_REQUEST;
              } else {
                pointsToConsume = LINCS_POINTS_PER_REQUEST;
              }
            } else {
              // not nerve or leaf-writer, but still an authenticated LINCS user
              pointsToConsume = LINCS_POINTS_PER_REQUEST;
            }
          }
        }
      }
    }
    logger.debug(`${key} consumed ${pointsToConsume} points`);
  } catch (error) {
    logger.error(getErrorMessage(error));
  }
  rateLimiter.consume(key, pointsToConsume)
    .then(() => {
      next();
    })
    .catch(() => {
      res.status(429).send('Too Many Requests');
    });
};
