import { SPARQL_ENDPOINT, SPARQL_PWD } from "@/globals";

export const execQuery = async (query: string) => {
  const response = await fetch(SPARQL_ENDPOINT + "/sparql", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded",  // Set the content type for POST
    },
    body: new URLSearchParams({ query: query }).toString(),  // Convert query parameters to URL-encoded string
  });
  if (response.ok) {
    const data = await response.json();
    return data.results.bindings;
  } else {
    const error = "Error from triplestore: " + await response.text();
    throw error;
  }
};

export const execUpdate = async (sparql: string) => {
  const base64Credentials = Buffer.from("admin:" + SPARQL_PWD).toString("base64");
  const response = await fetch(SPARQL_ENDPOINT + "/update", {
    method: "POST",
    headers: {
      Authorization: "Basic " + base64Credentials,
      "Content-Type": "application/sparql-update",
    },
    body: sparql,
  });
  return response;
};

export const filterByGraph = (graphs?: string[]): string => {
  if (graphs && graphs.length > 0) {
    let query = "FILTER(";
    for (const graph of graphs) {
      query += `CONTAINS(STR(?g), "${graph}") || `;
    }
    query = query.slice(0, -3);
    query += ")";
    return query;
  }
  return "";
};

export const filterPredicates = (predicates?: string[]): string => {
  if (predicates && predicates.length > 0) {
    let query = "FILTER(!(";
    for (const pred of predicates) {
      query += `CONTAINS(STR(?p), "${pred}") || `;
    }
    query = query.slice(0, -3);
    query += "))";
    return query;
  }
  return "";
}

export const labelsByLanguage = (resourceName: string, labelName: string, language: string): string => {
  // returns SPARQL to filter labels for ?varName
  // previously, we included the following in the sparql queries
  // that returned language and non-language labels for a resource:
  // ?varName rdfs:label ?valueName
  // FILTER (LANGMATCHES(LANG(?valueName), "en") || LANGMATCHES(LANG(?valueName), ""))

  // Return language labels if they exist.
  // Only return non-language labels if no language labels are found.
  return `
  {
    ?${resourceName} rdfs:label ?${labelName}
    FILTER (LANGMATCHES(LANG(?${labelName}), "${language}"))
  } UNION {
    ?${resourceName} rdfs:label ?${labelName}
    FILTER (LANGMATCHES(LANG(?${labelName}), ""))
    FILTER NOT EXISTS {
      ?${resourceName} rdfs:label ?langValue
      FILTER (LANGMATCHES(LANG(?langValue), "${language}"))
    }
  }
  `;
}

const lincs = "http://id.lincsproject.ca/";
const crm = "http://www.cidoc-crm.org/cidoc-crm/";
const foaf = "http://xmlns.com/foaf/0.1/";
const frbroo = "http://iflastandards.info/ns/fr/frbr/frbroo/";
const owl = "http://www.w3.org/2002/07/owl#";
const rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
const rdfs = "http://www.w3.org/2000/01/rdf-schema#";
const schema = "http://schema.org/";
const skos = "http://www.w3.org/2004/02/skos/core#";
const text = "http://jena.apache.org/text#";
const wd = "http://www.wikidata.org/entity/";
const oa = "http://www.w3.org/ns/oa#";

/* Commented prefixes to copy-paste in queries:
PREFIX lincs: <http://id.lincsproject.ca/>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <http://schema.org/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX text: <http://jena.apache.org/text#>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX oa: <http://www.w3.org/ns/oa#>
*/

export const PREFIX = {
  lincs: lincs,
  crm: crm,
  foaf: foaf,
  frbroo: frbroo,
  owl: owl,
  rdf: rdf,
  rdfs: rdfs,
  schema: schema,
  skos: skos,
  text: text,
  wd: wd,
  oa: oa
};

function createPrefixes() {
  let result = "";
  Object.keys(PREFIX).forEach(function (key: string) {
    //@ts-expect-error PREFIX key isn't typed as string
    result += `PREFIX ${key}: <${PREFIX[key]}> `;
  });
  return result.slice(0, -1);
}

export const PREFIXES = createPrefixes();

// Replace prefixes with keys
export function ReducePrefixes(input: string) {
  let result = input.valueOf();
  Object.keys(PREFIX).forEach(function (key) {
    //@ts-expect-error PREFIX key isn't typed as string
    result = result.replace(PREFIX[key], key + ":");
  });
  return result;
}

export const CONNECTION_COUNT_PRED = "http://id.lincsproject.ca/lincs/connectionCount";
export const CACHE_GRAPH = "http://graph.lincsproject.ca/cache";
export const PAGE_SIZE = 1000; // max number of triples to return per request