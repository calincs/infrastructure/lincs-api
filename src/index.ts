import { logger } from "@/helpers/logger";
import { getApplication } from "@/server";
import * as swaggerUi from "swagger-ui-express";
import { getDocument } from "@/documentation";
import { Request, Response, NextFunction } from "express";
import { PORT } from "./globals";

const app = getApplication(); // Express App singleton
export const SESSION_SECRET = process.env.SESSION_SECRET ?? "";

// Dynamically generate the swaggerDoc so we can pass the URL
app.use("/open-api", (req: Request, res: Response, next: NextFunction) => {
    const url = "localhost" === req.hostname ?
      `http://localhost:${PORT}` : `https://${req.hostname}`;
    //@ts-expect-error swagger Request object doesn't have TS defs
    req.swaggerDoc = getDocument(url);
    next();
  },
  swaggerUi.serve,
  swaggerUi.setup()
);
app.get("/", (req, res) => {
  res.redirect(301, "/open-api");
});

app.listen(PORT, () => {
  logger.info(`Listening at http://localhost:${PORT}`);
});
