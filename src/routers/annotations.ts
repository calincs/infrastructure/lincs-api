import { initServer } from "@ts-rest/express";
import { contracts, DOAnnotationQuery } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { execQuery } from "@/helpers/sparql";
import { buildQuery, createAnnotationsResponse } from "@/services/annotations";

const s = initServer();

export const annotationsRouter = s.router(contracts.api.annotations, {
  getAnnotations: {
    handler: async ({ body }) => {
      try {
        const query = buildQuery((body as DOAnnotationQuery));
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length === 0) {
          return {
            status: 204,
            body: [],
          };
        }
        const response = createAnnotationsResponse(data);
        return { status: 200, body: response };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
  getDOAnnotations: {
    handler: async ({ body }) => {
      try {
        const query = buildQuery(body);
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length === 0) {
          return {
            status: 204,
            body: [],
          };
        }
        const response = createAnnotationsResponse(data);
        return { status: 200, body: response };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
