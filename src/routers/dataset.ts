import { initServer } from "@ts-rest/express";
import { contracts } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { execQuery } from "@/helpers/sparql";
import { buildQuery, createResponse } from "@/services/dataset";

const s = initServer();

export const datasetRouter = s.router(contracts.api.dataset, {
  getDataset: {
    handler: async ({ body }) => {
      try {
        const query = buildQuery(body);
        logger.debug(query);
        const data = await execQuery(query);
        const response = createResponse(data);
        return { status: 200, body: response };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
