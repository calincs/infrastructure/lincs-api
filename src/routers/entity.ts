import { initServer } from "@ts-rest/express";
import { contracts } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { execQuery } from "@/helpers/sparql";
import { buildOutgoing, buildIncoming, createEntityResponse, buildExists, createExistsResponse, createIncomingResponse } from "@/services/entity";

const s = initServer();

export const entityRouter = s.router(contracts.api.entity, {
  getEntity: {
    handler: async ({ body }) => {
      try {
        // Build and execute the query
        const query = buildOutgoing(body);
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length === 0) {
          return {
            status: 204,
            body: [],
          };
        }
        const response = createEntityResponse(data);
        return { status: 200, body: response };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
  getIncoming: {
    handler: async ({ body }) => {
      try {
        // Build and execute the query
        const eventsQuery = buildIncoming(body);
        logger.debug(eventsQuery);
        const eventsData = await execQuery(eventsQuery);
        if (eventsData.length === 0) {
          return {
            status: 204,
            body: [],
          };
        }
        const response = createIncomingResponse(eventsData);
        return { status: 200, body: response };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
  findEntities: {
    handler: async ({ body }) => {
      try {
        const query = buildExists(body);
        logger.debug(query);
        const data = await execQuery(query);
        return { status: 200, body: createExistsResponse(data) };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
