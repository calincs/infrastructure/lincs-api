import { initServer } from '@ts-rest/express';
import { contracts } from '@lincs.project/lincs-api-contracts';
import { entityRouter } from './entity';
import { linkedRouter } from './linked';
import { linksRouter } from './links';
import { personRouter } from './person';
import { readTokenRouter } from './readToken';
import { placeRouter } from './place';
import { nerRouter } from './ner';
import { linkRouter } from './link';
import { annotationsRouter } from './annotations';
import { datasetRouter } from './dataset';
import { languageRouter } from './language';
import { workRouter } from './work';
import { groupRouter } from './group';

const s = initServer();

export const routers = s.router(contracts, {
  api: {
    annotations: annotationsRouter,
    dataset: datasetRouter,
    entity: entityRouter,
    group: groupRouter,
    language: languageRouter,
    link: linkRouter,
    linked: linkedRouter,
    links: linksRouter,
    ner: nerRouter,
    person: personRouter,
    place: placeRouter,
    readToken: readTokenRouter,
    work: workRouter,
  },
});
