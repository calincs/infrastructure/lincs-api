import { initServer } from "@ts-rest/express";
import { contracts } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import LanguageDetect from "languagedetect";

const s = initServer();

export const languageRouter = s.router(contracts.api.language, {
  getLanguages: {
    handler: async () => {
      try {
        return { status: 200, body: ["en", "fr"] };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
  getLanguage: {
    handler: async ({ body }) => {
      try {
        const detector = new LanguageDetect();
        detector.setLanguageType("iso2");
        const language = detector.detect(body.text)[0][0];
        logger.info(`Language detected: ${language}`);
        return { status: 200, body: { language: language } };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
