import { initServer } from "@ts-rest/express";
import { contracts } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { AUTHORITIES } from "@lincs.project/lincs-api-contracts";
import { linkEntity } from "@/services/link";

const s = initServer();

export const linkRouter = s.router(contracts.api.link, {
  getAuthorities: {
    handler: async () => {
      try {
        return { status: 200, body: Object.values(AUTHORITIES) };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
  reconcile: {
    handler: async ({ body }) => {
      try {
        const linkResults = await linkEntity(body);
        return { status: 200, body: linkResults };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
