import { initServer } from "@ts-rest/express";
import { contracts } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { execQuery, execUpdate } from "@/helpers/sparql";
import { createEntityResponse, buildQuery, countUpdateSparql } from "@/services/linked";

const s = initServer();

export const linkedRouter = s.router(contracts.api.linked, {
  getLinked: {
    handler: async ({ body }) => {
      try {
        // add some URI validation
        if (!body.uri || body.uri === "" || !body.uri.startsWith("http")) {
          return {
            status: 400,
            body: "A valid URI is required for this request",
          };
        }
        const query = buildQuery(body);
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length === 0) {
          return {
            status: 204,
            body: [],
          };
        }
        const response = createEntityResponse(data);
        // save entity count to the triplestore
        const statement = countUpdateSparql(body.uri, response.length);
        const updateResponse = await execUpdate(statement);
        if (!updateResponse.ok) {
          const errorBody = await updateResponse.text();
          logger.error("Can't create entity count statement: " + updateResponse.statusText);
          logger.error(errorBody);
        }
        return { status: 200, body: response };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
