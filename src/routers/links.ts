import { initServer } from "@ts-rest/express";
import { contracts } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { execQuery } from "@/helpers/sparql";
import { createEntityResponse, buildOutgoing } from "@/services/links";

const s = initServer();

export const linksRouter = s.router(contracts.api.links, {
  getLinks: {
    handler: async ({ body }) => {
      try {
        // add some URI validation
        if (!body.uri || body.uri === "" || !body.uri.startsWith("http")) {
          return {
            status: 400,
            body: "A valid URI is required for this request",
          };
        }
        const query = buildOutgoing(body);
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length === 0) {
          return {
            status: 204,
            body: [],
          };
        }
        const response = createEntityResponse(data);
        return { status: 200, body: response };
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
