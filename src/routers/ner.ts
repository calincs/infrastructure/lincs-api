import { initServer } from "@ts-rest/express";
import { NerEntities, contracts } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { SPACY_ENDPOINT } from "@/globals";

const s = initServer();

export const nerRouter = s.router(contracts.api.ner, {
  getEntities: {
    handler: async ({ body }) => {
      try {
        const spacyReq = { values: [{ recordId: "a1", text: body.text }], language: body.language };
        const PARAMS = {
          headers: { "Accept": "application/json", "Content-Type": "application/json" },
          method: "POST",
          body: JSON.stringify(spacyReq),
        };
        logger.debug(JSON.stringify(spacyReq));
        const response = await fetch(SPACY_ENDPOINT, PARAMS);
        const data = await response.json();
        if (response.status == 422) {
          return { status: 422, body: data };
        }
        if (response.status == 200) {
          const resp: NerEntities = {
            entities: data.values[0].data.entities,
            errors: data.values[0].errors,
            warnings: data.values[0].warnings,
          }
          return { status: 200, body: resp };
        }
        throw new Error(JSON.stringify(data));
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
