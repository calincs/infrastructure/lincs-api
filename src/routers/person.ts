import { initServer } from "@ts-rest/express";
import { contracts } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { execQuery } from "@/helpers/sparql";
import { buildQuery, createPerson, createStatements } from "@/services/person";

const s = initServer();

export const personRouter = s.router(contracts.api.person, {
  getPerson: {
    handler: async ({ body }) => {
      try {
        const query = buildQuery(body);
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length > 0) {
          return { status: 200, body: createPerson(data) };
        } else {
          return {
            status: 204,
            body: [],
          };
        }
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
  getPersonStatements: {
    handler: async ({ body }) => {
      try {
        const query = buildQuery(body);
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length > 0) {
          return { status: 200, body: createStatements(data) };
        } else {
          return {
            status: 204,
            body: [],
          };
        }
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
