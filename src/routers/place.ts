import { initServer } from "@ts-rest/express";
import { contracts, EntityStatement } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { execQuery } from "@/helpers/sparql";
import { buildQuery, createPlace, lookupGeoClass } from "@/services/place";
import { createStatements } from "@/services/person";

const s = initServer();

export const placeRouter = s.router(contracts.api.place, {
  getPlace: {
    handler: async ({ body }) => {
      try {
        const query = buildQuery(body);
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length > 0) {
          const result = createPlace(data);
          // lookup Geonames feature class if a location has a Geonames ID
          for (const place of result) {
            if (place.uri.startsWith("https://sws.geonames.org/")) {
              const geoId = place.uri.replace("https://sws.geonames.org/", "");
              place.featureClass = await lookupGeoClass(geoId);
            }
          }
          return { status: 200, body: result };
        } else {
          return {
            status: 204,
            body: [],
          };
        }
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
  getPlaceStatements: {
    handler: async ({ body }) => {
      try {
        const query = buildQuery(body);
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length > 0) {
          const result = createStatements(data);
          // lookup Geonames feature class if a location has a Geonames ID
          for (const place of result) {
            if (place.uri.startsWith("https://sws.geonames.org/")) {
              const geoId = place.uri.replace("https://sws.geonames.org/", "");
              const featureClass = await lookupGeoClass(geoId);
              const statement:EntityStatement = {
                type: "featureClass",
                graph: "http://www.geonames.org",
                uri: `http://www.geonames.org/ontology#${featureClass}`,
                value: featureClass,
              };
              place.statements.push(statement);
            }
          }
          return { status: 200, body: result };
        } else {
          return {
            status: 204,
            body: [],
          };
        }
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
