import { initServer } from "@ts-rest/express";
import { contracts, TokenResponse } from "@lincs.project/lincs-api-contracts";
import { KC } from "@/helpers/kc-connect";
import jsonwebtoken from "jsonwebtoken";
import createError from "http-errors";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";

const s = initServer();

export const readTokenRouter = s.router(contracts.api.readToken, {
  getToken: {
    middleware: [KC.protect()],
    handler: async ({ headers: { authorization } }) => {
      if (authorization) {
        const response = await decodeToken(authorization);
        if (response instanceof createError.HttpError) {
          return {
            status: response.status,
            body: { message: response.message },
          };
        }
        return { status: 200, body: response };
      }
      return {
        status: 400,
        body: { message: "No Authorization token available." },
      };
    },
  },
});

const decodeToken = async (authToken: string) => {
  try {
    const token = jsonwebtoken.decode(authToken.substring(7)); // remove "Bearer " prefix
    if (!token || typeof token === "string") {
      return createError(500, getErrorMessage("Invalid Auth Token got past KC.protect()!"));
    } else {
      // token decode successful
      const tokenResponse: TokenResponse = {
        clientId: token.azp,
        userSession: token.session_state,
        username: token.preferred_username,
        email: token.email,
        realmRoles: token.realm_access.roles,
      };
      return tokenResponse;
    }
  } catch (error) {
    logger.error(getErrorMessage(error));
    return createError(500, getErrorMessage(error));
  }
};
