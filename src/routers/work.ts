import { initServer } from "@ts-rest/express";
import { contracts } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { execQuery } from "@/helpers/sparql";
import { buildQuery, createWork } from "@/services/work";
import { createStatements } from "@/services/person";

const s = initServer();

export const workRouter = s.router(contracts.api.work, {
  getWork: {
    handler: async ({ body }) => {
      try {
        const query = buildQuery(body);
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length > 0) {
          return { status: 200, body: createWork(data) };
        } else {
          return {
            status: 204,
            body: [],
          };
        }
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
  getWorkStatements: {
    handler: async ({ body }) => {
      try {
        const query = buildQuery(body);
        logger.debug(query);
        const data = await execQuery(query);
        if (data.length > 0) {
          const result = createStatements(data);
          return { status: 200, body: result };
        } else {
          return {
            status: 204,
            body: [],
          };
        }
      } catch (error) {
        logger.error(getErrorMessage(error));
        return {
          status: 500,
          body: { message: getErrorMessage(error) },
        };
      }
    },
  },
});
