import express, { Express } from "express";
import cors from "cors";
import { KC, Session } from "@/helpers/kc-connect";
import { loggerMiddleware, logger } from "@/helpers/logger";
import { createExpressEndpoints } from "@ts-rest/express";
import { contracts } from "@lincs.project/lincs-api-contracts";
import { routers } from "@/routers";
import { getErrorMessage } from "@/helpers/error";
import { rateLimiterMiddleware } from "@/helpers/rateLimiter";

// Singleton for our Express app
let app: Express;

export function getApplication(): Express {
  if (!app) {
    const app = express();
    try {
      app.use(express.json({ limit: "10mb" }));
      app.use(cors());
      app.use(loggerMiddleware);
      app.use(Session);
      app.use(KC.middleware());
      // Ensure that client IP addresses are identified correctly
      app.set('trust proxy', true);
      app.use(rateLimiterMiddleware);
      app.get("/healthz", (req, res) => res.send("OK"));
      // Create all endpoints in /routers using ts-rest contracts
      createExpressEndpoints(contracts, routers, app);
      return app;
    } catch (error) {
      logger.error(getErrorMessage(error));
      throw error;
    }
  }
  return app;
}
