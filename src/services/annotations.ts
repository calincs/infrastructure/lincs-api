import { FilteredEntityQuery, AnnotationsResponse, Annotation, Resource, DigitalObject, DOAnnotationQuery } from "@lincs.project/lincs-api-contracts";
import { PAGE_SIZE, PREFIXES, filterByGraph } from "@/helpers/sparql";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createAnnotationsResponse(data: any[]): AnnotationsResponse {
  // Group data into arrays. Possible to have multiple DOs with multiple sources
  const results: AnnotationsResponse = [];
  let currAnnotation: Annotation = { annotation: "" };
  let currDO: DigitalObject = {};
  data.map((row) => {
    if (row.wa.value != currAnnotation.annotation) {
      if (currAnnotation.annotation) {
        if (currDO.uri) currAnnotation.digitalObjects?.push(currDO);
        // init DO to empty object, will re-init below
        currDO = {};
        results.push(currAnnotation);
      }
      // init new annotation
      currAnnotation = { annotation: row.wa.value, digitalObjects: [] };
      if (row.g) currAnnotation.graph = row.g.value;
      if (row.waLabel) currAnnotation.annotationLabel = row.waLabel.value;
      if (row.e55) currAnnotation.e55 = row.e55.value;
      if (row.info) currAnnotation.info = row.info.value;
      if (row.infoLabel) currAnnotation.infoLabel = row.infoLabel.value;
      if (row.quoteText) currAnnotation.quote = row.quoteText.value;
    }
    if (row.digObject && row.digObject.value != currDO.uri) {
      if (currDO.uri) currAnnotation.digitalObjects?.push(currDO);
      // init new DO
      currDO = { uri: row.digObject.value };
      if (row.doLabel) currDO.label = row.doLabel.value;
    }
    const record: Resource = {};
    if (row.dataSource) record.uri = row.dataSource.value;
    if (row.dsLabel) record.label = row.dsLabel.value;
    if (record.uri) {
      if (!currDO.sources) currDO.sources = [];
      currDO.sources.push(record);
    }
  });
  if (currDO.uri) currAnnotation.digitalObjects?.push(currDO);
  if (currAnnotation.annotation) results.push(currAnnotation);
  return results;
}

export function buildQuery(body: DOAnnotationQuery): string {
  let query = PREFIXES;
  query += `
SELECT DISTINCT ?g ?wa (MAX(?wa_label) AS ?waLabel)
  (GROUP_CONCAT(DISTINCT ?type_label; separator="; ") AS ?e55)
  ?info (MAX(?info_label) AS ?infoLabel)
  (SAMPLE(?quote_text) AS ?quoteText)
  (SAMPLE(?do) AS ?digObject)
  (MAX(?do_label) AS ?doLabel)
  (SAMPLE(?data_source) AS ?dataSource)
  (MAX(?ds_label) AS ?dsLabel)
WHERE {
  GRAPH ?g {
    ?wa oa:hasBody <${body.uri}> .
    ?wa oa:hasTarget ?info .
    ?info oa:hasSelector ?selector .`;
  if (body.doUri) query += `
    ?info oa:hasSource <${body.doUri}> .`;
  query += `
  }
  OPTIONAL {
    ?wa crm:P2_has_type ?e55_type .
    OPTIONAL {
      ?e55_type rdfs:label ?type_label .
      FILTER(LANGMATCHES(LANG(?type_label), "${body.language}"))
    }
    OPTIONAL {
      ?e55_type rdfs:label ?type_label .
      FILTER(LANGMATCHES(LANG(?type_label), ""))
    }
    OPTIONAL {
      ?e55_type rdfs:label ?type_label .
      FILTER(LANGMATCHES(LANG(?type_label), "en"))
    }
  }
  OPTIONAL {
    ?info oa:hasSource ?do .
    OPTIONAL {
      ?do rdfs:label ?do_label .
      FILTER(LANGMATCHES(LANG(?do_label), "${body.language}"))
    }
    OPTIONAL {
      ?do rdfs:label ?do_label .
      FILTER(LANGMATCHES(LANG(?do_label), ""))
    }
    OPTIONAL {
      ?do rdfs:label ?do_label .
      FILTER(LANGMATCHES(LANG(?do_label), "en"))
    }
    OPTIONAL {
      ?do crm:P1_is_identified_by ?lingApp .
      ?lingApp crm:P190_has_symbolic_content ?do_label .
      FILTER(LANGMATCHES(LANG(?do_label), "${body.language}"))
    }
    OPTIONAL {
      ?do crm:P1_is_identified_by ?lingApp .
      ?lingApp crm:P190_has_symbolic_content ?do_label .
      FILTER(LANGMATCHES(LANG(?do_label), ""))
    }
    OPTIONAL {
      ?do crm:P1_is_identified_by ?lingApp .
      ?lingApp crm:P190_has_symbolic_content ?do_label .
      FILTER(LANGMATCHES(LANG(?do_label), "en"))
    }
    OPTIONAL {
      ?do crm:P67_refers_to | crm:P1_is_identified_by ?data_source .
      OPTIONAL {
        ?data_source rdfs:label | crm:P190_has_symbolic_content ?ds_label .
        FILTER(LANGMATCHES(LANG(?ds_label), "${body.language}"))
      }
      OPTIONAL {
        ?data_source rdfs:label | crm:P190_has_symbolic_content ?ds_label .
        FILTER(LANGMATCHES(LANG(?ds_label), ""))
      }
      OPTIONAL {
        ?data_source rdfs:label | crm:P190_has_symbolic_content ?ds_label .
        FILTER(LANGMATCHES(LANG(?ds_label), "en"))
      }
    }
  }
  OPTIONAL {
    ?selector oa:exact ?quote_text .
  }
  OPTIONAL {
    ?selector oa:refinedBy ?quote .
    ?quote oa:exact ?quote_text .
  }
  OPTIONAL {
    ?selector oa:prefix ?prefix .
    ?selector oa:suffix ?suffix .
    ?selector oa:exact ?match .
    BIND(CONCAT(CONCAT(?prefix, ?match), ?suffix) as ?quote_text)
  }
  OPTIONAL {
    ?wa rdfs:label ?wa_label .
    FILTER(LANGMATCHES(LANG(?wa_label), "${body.language}"))
  }
  OPTIONAL {
    ?wa rdfs:label ?wa_label .
    FILTER(LANGMATCHES(LANG(?wa_label), ""))
  }
  OPTIONAL {
    ?wa rdfs:label ?wa_label .
    FILTER(LANGMATCHES(LANG(?wa_label), "en"))
  }
  OPTIONAL {
    ?info rdfs:label ?info_label .
    FILTER(LANGMATCHES(LANG(?info_label), "${body.language}"))
  }
  OPTIONAL {
    ?info rdfs:label ?info_label .
    FILTER(LANGMATCHES(LANG(?info_label), ""))
  }
  OPTIONAL {
    ?info rdfs:label ?info_label .
    FILTER(LANGMATCHES(LANG(?info_label), "en"))
  }
  `;
  query += filterByGraph(body.graphs) + `
}
GROUP BY ?g ?wa ?info ?data_source
ORDER BY ?wa ?info ?do ?dataSource
LIMIT ${PAGE_SIZE} OFFSET ${(body.page - 1) * PAGE_SIZE}
  `;
  return query;
}
