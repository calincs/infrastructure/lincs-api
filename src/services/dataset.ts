import { SimpleEntityQuery, SimpleStatement, SimpleStatementResponse } from "@lincs.project/lincs-api-contracts";
import { PREFIXES } from "@/helpers/sparql";
import { DatasetQuery } from "@lincs.project/lincs-api-contracts/index";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createResponse(data: any[]): SimpleStatementResponse {
  const results: SimpleStatementResponse = [];
  data.map((row) => {
    const record: SimpleStatement = { predicate: row.p.value };
    if (row.pLabel) record.predicateLabel = row.pLabel.value;
    if (row.o) record.object = row.o.value;
    if (row.oLabel) record.objectLabel = row.oLabel.value;
    if (row.oType) record.type = row.oType.value;
    if (row.tLabel) record.typeLabel = row.tLabel.value;
    results.push(record);
  });
  return results;
}

export function buildQuery(body: DatasetQuery) {
  let query = PREFIXES;
  query += `
  SELECT ?p ?o (MAX(?pl) AS ?pLabel) (MAX(?ol) AS ?oLabel) (SAMPLE(?type) AS ?oType) (MAX(?tl) AS ?tLabel) WHERE {
    GRAPH <http://metadata.lincsproject.ca> {
      <${body.uri}> ?p ?o .
    }
    OPTIONAL {
      ?p rdfs:label ?pl
      FILTER (LANGMATCHES(LANG(?pl), "${body.language}"))
    }
    OPTIONAL {
      ?p rdfs:label ?pl
      FILTER (LANGMATCHES(LANG(?pl), ""))
    }
    OPTIONAL {
      ?p rdfs:label ?pl
      FILTER (LANGMATCHES(LANG(?pl), "en"))
    }
    OPTIONAL {
      ?o rdfs:label ?ol
      FILTER (LANGMATCHES(LANG(?ol), "${body.language}"))
    }
    OPTIONAL {
      ?o rdfs:label ?ol
      FILTER (LANGMATCHES(LANG(?ol), ""))
    }
    OPTIONAL {
      ?o rdfs:label ?ol
      FILTER (LANGMATCHES(LANG(?ol), "en"))
    }
    OPTIONAL {
      ?o rdf:type ?type .
      OPTIONAL {
        ?type rdfs:label ?tl .
        FILTER (LANGMATCHES(LANG(?tl), "${body.language}"))
      }
      OPTIONAL {
        ?type rdfs:label ?tl .
        FILTER (LANGMATCHES(LANG(?tl), ""))
      }
      OPTIONAL {
        ?type rdfs:label ?tl .
        FILTER (LANGMATCHES(LANG(?tl), "en"))
      }
    }
  }
  GROUP BY ?p ?o
  ORDER BY ?p ?o  
  `;
  return query;
}

