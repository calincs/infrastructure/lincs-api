import { EntityResponse, EntityQuery, Statement, Entity, SimpleEntity, FilteredEntityQuery } from "@lincs.project/lincs-api-contracts";
import { PREFIXES, PREFIX, filterByGraph, PAGE_SIZE } from "@/helpers/sparql";

// expands entity properties by calling extractLabelAndType() for each entity
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createEntityResponse(outgoingData: any[]): EntityResponse {
  const results = formatOutgoing(outgoingData);
  // respond with extracted labels and types
  return expandEntities(results);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createIncomingResponse(incomingData: any[]): EntityResponse {
  const results = formatIncoming(incomingData);
  // respond with extracted labels and types
  return expandEntities(results);
}

// expands entity properties by calling extractStatementProperties() for each entity
export function expandEntities(entities: EntityResponse): EntityResponse {
  const results: EntityResponse = [];
  for (const entity of entities) {
    const statement = extractStatementProperties(entity);
    if (entity.incoming) {
      statement.incoming = [];
      for (const event of entity.incoming) {
        statement.incoming.push(extractStatementProperties(event));
      }
    }
    results.push(statement);
  }
  return results;
}

// returns an Entity with the type, label, and date properties extracted from outgoing statements
function extractStatementProperties(entity: Entity): Entity {
  const result: Entity = { resource: entity.resource };
  const outgoing: Statement[] = [];
  for (const statement of entity.outgoing ?? []) {
    if (statement.predicate === `${PREFIX.rdfs}label`) {
      result.resourceLabel = statement.object;
      result.graph = statement.graph;
    } else if (statement.predicate === `${PREFIX.rdf}type`) {
      result.type = statement.object;
      result.typeLabel = statement.objectLabel;
    } else if (statement.predicate === `${PREFIX.crm}P4_has_time-span`) {
      result.startDate = statement.startDate;
      result.endDate = statement.endDate;
    } else {
      outgoing.push(statement);
    }
  }
  if (outgoing.length > 0) result.outgoing = outgoing;
  return result;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function formatOutgoing(data: any[]): EntityResponse {
  const results: EntityResponse = [];
  let currEntity: Entity = { resource: "" };
  data.map((row) => {
    if (row.s.value != currEntity.resource) {
      if (currEntity.resource) results.push(currEntity);
      // init new entity
      currEntity = { resource: row.s.value };
    }
    const record: Statement = {
      graph: row.g.value,
      predicate: row.p.value,
    };
    if (row.pLabel) record.predicateLabel = row.pLabel.value;
    if (row.o) record.object = row.o.value;
    if (row.oLabel) record.objectLabel = row.oLabel.value;
    if (record.object && record.object.startsWith("http")) record.objectIsURI = true;
    else record.objectIsURI = false;
    if (row.oType) record.type = row.oType.value;
    if (row.tLabel) record.typeLabel = row.tLabel.value;
    if (row.startDate) record.startDate = row.startDate.value;
    if (row.endDate) record.endDate = row.endDate.value;
    if (!currEntity.outgoing) currEntity.outgoing = [];
    currEntity.outgoing.push(record);
  });
  if (currEntity.resource) results.push(currEntity);
  return results;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function formatIncoming(data: any[]): EntityResponse {
  const results: EntityResponse = [];
  let currEntity: Entity = { resource: "" };
  let currEvent: SimpleEntity = { resource: "" };
  data.map((row) => {
    if (row.o.value != currEntity.resource) {
      // new entity
      currEntity = { resource: row.o.value };
      results.push(currEntity);
    }
    if (row.s.value != currEvent.resource) {
      // new event
      if (currEvent.resource) {
        if (!currEntity.incoming) currEntity.incoming = [];
        currEntity.incoming.push(currEvent);
      }
      // init new entity
      currEvent = { resource: row.s.value };
    }
    const record: Statement = {
      graph: row.g.value,
      predicate: row.pp.value,
    };
    if (row.ppLabel) record.predicateLabel = row.ppLabel.value;
    if (row.oo) record.object = row.oo.value;
    if (row.ooLabel) record.objectLabel = row.ooLabel.value;
    // Check if the object is just data or if it is a URI
    if (record.object && record.object.startsWith("http")) record.objectIsURI = true;
    else record.objectIsURI = false;
    if (row.ooType) record.type = row.ooType.value;
    if (row.tLabel) record.typeLabel = row.tLabel.value;
    if (!currEvent.outgoing) currEvent.outgoing = [];
    currEvent.outgoing.push(record);
  });
  if (!currEntity.incoming) currEntity.incoming = [];
  if (currEvent.resource) currEntity.incoming.push(currEvent);
  if (currEntity.resource) results.push(currEntity);
  return results;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createExistsResponse(entities: any[]): string[] {
  const response: string[] = [];
  for(const entity of entities) {
    response.push(entity.s.value);
  }
  return response;
}

export function buildExists(body: string[]) {
  let query = PREFIXES;
  query += `
SELECT DISTINCT ?s
WHERE {
  VALUES ?s {
    `;
    for (const uri of body) {
      query += `<${uri}> `;
    }
    query += `
  }
  ?s ?p ?o .
}
  `;
  return query;
}

export function buildOutgoing(body: EntityQuery) {
  let query = PREFIXES;
  query += `
SELECT DISTINCT ?g ?s ?p ?o (MAX(?pl) AS ?pLabel) (MAX(?ol) AS ?oLabel) (SAMPLE(?type) AS ?oType) (MAX(?tl) AS ?tLabel) WHERE {
  GRAPH ?g {
    VALUES (?s) { `;
  for (const uri of body.uris) {
    query += `(<${uri}>) `;
  }
  query += `
    }
    ?s ?p ?temp;
    OPTIONAL {
      ?s ?p ?o .
      FILTER(!ISLITERAL(?o))
    }
    OPTIONAL {
      ?s ?p ?o .
      FILTER(LANGMATCHES(LANG(?o), "${body.language}"))
    }
    OPTIONAL {
      ?s ?p ?o .
      FILTER(LANGMATCHES(LANG(?o), ""))
    }
    OPTIONAL {
      ?s ?p ?o .
      FILTER(LANGMATCHES(LANG(?o), "en"))
    }
  }
  OPTIONAL {
    ?p rdfs:label ?pl
    FILTER (LANGMATCHES(LANG(?pl), "${body.language}"))
  }
  OPTIONAL {
    ?p rdfs:label ?pl
    FILTER (LANGMATCHES(LANG(?pl), ""))
  }
  OPTIONAL {
    ?p rdfs:label ?pl
    FILTER (LANGMATCHES(LANG(?pl), "en"))
  }
  OPTIONAL {
    ?o rdfs:label ?ol
    FILTER (LANGMATCHES(LANG(?ol), "${body.language}"))
  }
  OPTIONAL {
    ?o rdfs:label ?ol
    FILTER (LANGMATCHES(LANG(?ol), ""))
  }
  OPTIONAL {
    ?o rdfs:label ?ol
    FILTER (LANGMATCHES(LANG(?ol), "en"))
  }
  OPTIONAL {
    ?o rdf:type ?type .
    OPTIONAL {
      ?type rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), "${body.language}"))
    }
    OPTIONAL {
      ?type rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), ""))
    }
    OPTIONAL {
      ?type rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), "en"))
    }
  }
  `;
  query += filterByGraph(body.graphs);
  if (body.language === "en") {
    query += `
    FILTER (!isLiteral(?o) || LANGMATCHES(LANG(?o), "${body.language}") || LANGMATCHES(LANG(?o), ""))`
  }
  query += `
}
GROUP BY ?g ?s ?p ?o
ORDER BY ?s ?oLabel
LIMIT ${PAGE_SIZE} OFFSET ${(body.page - 1) * PAGE_SIZE}
  `;
  return query;
}

export function buildIncoming(body: FilteredEntityQuery) {
  let query = PREFIXES;
  query += `
SELECT ?g ?s ?o ?pp (SAMPLE(?ppl) AS ?ppLabel) ?oo (SAMPLE(?ool) AS ?ooLabel) (SAMPLE(?type) AS ?ooType) (SAMPLE(?tl) AS ?tLabel) WHERE {
  GRAPH ?g {
    BIND (<${body.uri}> as ?o)
    ?s ?p ?o.
    ?s ?pp ?oo
  }
  OPTIONAL {
    ?pp rdfs:label ?ppl
    FILTER (LANGMATCHES(LANG(?ppl), "${body.language}"))
  }
  OPTIONAL {
    ?pp rdfs:label ?ppl
    FILTER (LANGMATCHES(LANG(?ppl), ""))
  }
  OPTIONAL {
    ?pp rdfs:label ?ppl
    FILTER (LANGMATCHES(LANG(?ppl), "en"))
  }
  OPTIONAL {
    ?oo rdfs:label ?ool
    FILTER (LANGMATCHES(LANG(?ool), "${body.language}"))
  }
  OPTIONAL {
    ?oo rdfs:label ?ool
    FILTER (LANGMATCHES(LANG(?ool), ""))
  }
  OPTIONAL {
    ?oo rdfs:label ?ool
    FILTER (LANGMATCHES(LANG(?ool), "en"))
  }
  OPTIONAL {
    ?oo rdf:type ?type .
    OPTIONAL {
      ?type rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), "${body.language}"))
    }
    OPTIONAL {
      ?type rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), ""))
    }
    OPTIONAL {
      ?type rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), "en"))
    }
  }
  `;
  query += filterByGraph(body.graphs) + `
}
GROUP BY ?g ?s ?o ?pp ?oo
ORDER BY ?o ?s ?ooLabel
LIMIT ${PAGE_SIZE} OFFSET ${(body.page - 1) * PAGE_SIZE}
  `;
  return query;
}
