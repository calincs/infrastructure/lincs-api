import { Resource, SimpleEntityQuery, Group, EntityStatements, EntityStatement } from "@lincs.project/lincs-api-contracts";
import { PREFIXES } from "@/helpers/sparql";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createGroup(data: any[]): Group[] {
  const results: Group[] = [];
  let currGroup: Group = { uri: "" };
  data.map((row) => {
    if (row.entity.value != currGroup.uri) {
      if (currGroup.uri) {
        currGroup.types?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
        currGroup.members?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
        currGroup.works?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
        results.push(currGroup);
      }
      // init new group
      currGroup = { uri: row.entity.value };
    }
    const resource: Resource = {
      uri: row.resource?.value,
      label: row.value?.value,
    };
    if ("label" === row.type.value) {
      // Write new label if current label is empty or if the new label comes after the current one alphabetically
      if (!currGroup.label || (resource.label && resource.label > currGroup.label)) {
        currGroup.graph = row.g?.value;
        currGroup.label = resource.label;
      }
    }
    if ("type" === row.type.value) {
      if (!currGroup.types) currGroup.types = [];
      const index = currGroup.types.findIndex(element => element.uri === row.resource.value);
      if (index === -1) {
        currGroup.types.push(resource);
      } else {
        currGroup.types[index] = resource;
      }
    }
    if ("member" === row.type.value) {
      if (!currGroup.members) currGroup.members = [];
      const index = currGroup.members.findIndex(element => element.uri === row.resource.value);
      if (index === -1) {
        currGroup.members.push(resource);
      } else {
        currGroup.members[index] = resource;
      }
    }
    if ("work" === row.type.value) {
      if (!currGroup.works) currGroup.works = [];
      const index = currGroup.works.findIndex(element => element.uri === row.resource.value);
      if (index === -1) {
        currGroup.works.push(resource);
      } else {
        currGroup.works[index] = resource;
      }
    }
  });

  if (currGroup.uri) {
    currGroup.types?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
    currGroup.members?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
    currGroup.works?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
    results.push(currGroup);
  }
  return results;
}

export function buildQuery(body: SimpleEntityQuery) {
  let query = PREFIXES;
  query += `
SELECT DISTINCT ?entity ?type ?resource ?value ?g WHERE {
  VALUES (?entity) { `;
  for (const uri of body.uris) {
    query += `(<${uri}>) `;
  }
  query += `
  }
  GRAPH ?g {
    {
      BIND('label' as ?type)
      ?entity rdfs:label ?temp .
      OPTIONAL {
        ?entity rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
        ?entity rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
        ?entity rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    } UNION {
      BIND('type' as ?type)
      ?entity crm:P2_has_type ?resource .
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    } UNION {
      BIND('member' as ?type)
      ?entity crm:P107_has_current_or_former_member ?resource .
      OPTIONAL {
      ?resource rdfs:label ?value
      FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
      ?resource rdfs:label ?value
      FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
      ?resource rdfs:label ?value
      FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    } UNION {
      BIND('work' as ?type)
      {
        ?range crm:P02_has_range ?entity .
        ?range crm:P01_has_domain ?event .
        ?event crm:P94_has_created ?resource .
      }
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    }
  }
}
  `;
  return query;
}
