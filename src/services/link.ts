import { LinkMatch, LinkRequest, LinkResult } from "@lincs.project/lincs-api-contracts";
import { AUTHORITIES } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { logger } from "@/helpers/logger";
import { GEONAMES_ENDPOINT } from "@/globals";
import { WBK } from "wikibase-sdk";
import xml2js from "xml2js";

export async function linkEntity(request: LinkRequest) {
  logger.debug(request);
  const numEntities = request.moreResults ? 10 : 4;
  const results: LinkResult[] = [];
  for (const authority of request.authorities ?? []) {
    const linkResult: LinkResult = { authority: authority };
    // Dbpedia
    if (authority === AUTHORITIES.DBpedia_All) linkResult.matches = await dbpedia(request.entity, undefined, numEntities);
    if (authority === AUTHORITIES.DBpedia_Event) linkResult.matches = await dbpedia(request.entity, "Event", numEntities);
    if (authority === AUTHORITIES.DBpedia_Organisation) linkResult.matches = await dbpedia(request.entity, "Organisation", numEntities);
    if (authority === AUTHORITIES.DBpedia_Person) linkResult.matches = await dbpedia(request.entity, "Person", numEntities);
    if (authority === AUTHORITIES.DBpedia_Place) linkResult.matches = await dbpedia(request.entity, "Place", numEntities);
    if (authority === AUTHORITIES.DBpedia_Work) linkResult.matches = await dbpedia(request.entity, "Work", numEntities);
    // Geonames
    if (authority === AUTHORITIES.GeoNames) linkResult.matches = await geonames(request.entity);
    //Getty
    if (authority === AUTHORITIES.Getty_AAT)
      linkResult.matches = await getty(request.entity, "/aat", numEntities);
    if (authority === AUTHORITIES.Getty_CONA)
      linkResult.matches = await gettyCONA(request.entity, numEntities);
    if (authority === AUTHORITIES.Getty_TGN)
      linkResult.matches = await getty(request.entity, "/tgn", numEntities);
    if (authority === AUTHORITIES.Getty_ULAN)
      linkResult.matches = await getty(request.entity, "/ulan", numEntities);
    if (authority === AUTHORITIES.Getty_ALL)
      linkResult.matches = await getty(request.entity, "/all", numEntities);
    // GND
    if (authority === AUTHORITIES.GND_Organisation)
      linkResult.matches = await gnd(request.entity, "CoporateBody", numEntities);
    if (authority === AUTHORITIES.GND_Person)
      linkResult.matches = await gnd(request.entity, "Person", numEntities);
    if (authority === AUTHORITIES.GND_Place)
      linkResult.matches = await gnd(request.entity, "PlaceOrGeographicName", numEntities);
    if (authority === AUTHORITIES.GND_Subject)
      linkResult.matches = await gnd(request.entity, "SubjectHeading", numEntities);
    if (authority === AUTHORITIES.GND_Work)
      linkResult.matches = await gnd(request.entity, "Work", numEntities);
    // LINCS
    if (authority === AUTHORITIES.LINCS_ALL || authority === "LINCS") linkResult.matches = await lincs(request.entity, undefined, numEntities);
    if (authority === AUTHORITIES.LINCS_Event) linkResult.matches = await lincs(request.entity, ["http://www.cidoc-crm.org/cidoc-crm/E5_Event", "http://www.cidoc-crm.org/cidoc-crm/E7_Activity", "http://www.cidoc-crm.org/cidoc-crm/E8_Acquisition", "http://www.cidoc-crm.org/cidoc-crm/E12_Production", "http://www.cidoc-crm.org/cidoc-crm/E13_Attribute_Assignment", "http://www.cidoc-crm.org/cidoc-crm/E65_Creation", "http://www.cidoc-crm.org/cidoc-crm/E66_Formation", "http://www.cidoc-crm.org/cidoc-crm/E67_Birth", "http://www.cidoc-crm.org/cidoc-crm/E69_Death", "http://www.cidoc-crm.org/cidoc-crm/E85_Joining", "http://www.cidoc-crm.org/cidoc-crm/crmtex/TX6_Transcription", "http://iflastandards.info/ns/fr/frbr/frbroo/F29_Recording_Event", "http://iflastandards.info/ns/fr/frbr/frbroo/F31_Performance", "http://iflastandards.info/ns/fr/frbr/frbroo/F51_Pursuit"], numEntities);
    if (authority === AUTHORITIES.LINCS_Group) linkResult.matches = await lincs(request.entity, "http://www.cidoc-crm.org/cidoc-crm/E74_Group", numEntities);
    if (authority === AUTHORITIES.LINCS_Person) linkResult.matches = await lincs(request.entity, "http://www.cidoc-crm.org/cidoc-crm/E21_Person", numEntities);
    if (authority === AUTHORITIES.LINCS_Place) linkResult.matches = await lincs(request.entity, "http://www.cidoc-crm.org/cidoc-crm/E53_Place", numEntities);
    if (authority === AUTHORITIES.LINCS_Work) linkResult.matches = await lincs(request.entity, ["http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work", "http://iflastandards.info/ns/fr/frbr/frbroo/F2_Expression", "http://www.wikidata.org/entity/Q15306849"], numEntities);
    // VIAF
    if (authority === AUTHORITIES.VIAF_Bibliographic)
      linkResult.matches = await viaf(request.entity, "local.title", numEntities);
    if (authority === AUTHORITIES.VIAF_Corporate)
      linkResult.matches = await viaf(request.entity, "local.corporateNames", numEntities);
    if (authority === AUTHORITIES.VIAF_Expressions)
      linkResult.matches = await viaf(request.entity, "local.uniformTitleExpressions", numEntities);
    if (authority === AUTHORITIES.VIAF_Geographic)
      linkResult.matches = await viaf(request.entity, "local.geographicNames", numEntities);
    if (authority === AUTHORITIES.VIAF_Personal)
      linkResult.matches = await viaf(request.entity, "local.personalNames", numEntities);
    if (authority === AUTHORITIES.VIAF_Works)
      linkResult.matches = await viaf(request.entity, "local.uniformTitleWorks", numEntities);
    // Wikidata
    if (authority === AUTHORITIES.Wikidata) linkResult.matches = await wikidata(request.entity, numEntities);
    results.push(linkResult);
  }
  return results;
}

async function dbpedia(entity: string, typeName: string | undefined, numEntities: number): Promise<LinkMatch[]> {
  const results: LinkMatch[] = [];
  if (!entity) {
    return Promise.reject("Cannot link to empty entity");
  }
  let req_url = `https://lookup.dbpedia.org/api/search?maxResults=${numEntities}&format=JSON_RAW&query=${entity}`;
  if (typeName) req_url = req_url + `&typeName=${typeName}`;
  const params = {
    headers: { Accept: "application/json" },
    method: "GET",
  };
  try {
    const response = await fetch(req_url, params);
    const data = await response.json();
    if (response.ok) {
      //@ts-expect-error no ts defs for api result
      data.docs.map((candidate) => {
        results.push({
          uri: candidate.resource[0],
          label: candidate.label[0],
          description: candidate.comment[0],
        });
      });
    } else {
      return Promise.reject("DBpedia: " + response.text());
    }
  } catch (error) {
    return Promise.reject("DBpedia: " + getErrorMessage(error));
  }
  return results;
}

async function geonames(entity: string): Promise<LinkMatch[]> {
  const results: LinkMatch[] = [];
  if (!entity) {
    return Promise.reject("Cannot link to empty entity");
  }
  const searchParam = entity; // entity.slice(0, -1); // index works more consistently without the last char in word
  const req_url = `${GEONAMES_ENDPOINT}?search=${searchParam}`;
  const params = {
    headers: { Accept: "application/json" },
    method: "GET",
  };
  try {
    const response = await fetch(req_url, params);
    const data = await response.json();
    if (response.ok && !data.Error) {
      //@ts-expect-error no ts defs for api result
      data.data.map((candidate) => {
        let description = candidate.properties.admin1;
        if (candidate.properties.admin2) description = description + ", " + candidate.properties.admin2;
        if (candidate.properties.iso2) description = description + ", " + candidate.properties.iso2;
        results.push({
          uri: `https://sws.geonames.org/${candidate.properties.id}`,
          label: candidate.properties.name,
          description: description,
        });
      });
    } else {
      return Promise.reject("GeoNames: " + JSON.stringify(data));
    }
  } catch (error) {
    return Promise.reject("GeoNames: " + getErrorMessage(error));
  }
  return results;
}

async function getty(entity: string, queryType: string, numEntities: number) {
  const results: LinkMatch[] = [];
  const req_url = "https://services.getty.edu/vocab/reconcile";
  const req_body = { q1: { query: entity, type: queryType, limit: numEntities } };
  const searchParams = new URLSearchParams({ queries: JSON.stringify(req_body) });
  const params = {
    headers: { Accept: "application/json", "Content-Type": "application/x-www-form-urlencoded" },
    method: "POST",
    body: searchParams.toString(),
  };
  try {
    const response = await fetch(req_url, params);
    const data = await response.json();
    if (response.ok && !data.Error) {
      //@ts-expect-error no ts defs for api result
      data.q1.result.sort(function (a, b) {
        return b.score - a.score;
      });
      //@ts-expect-error no ts defs for api result
      data.q1.result.slice(0, numEntities).map((candidate) => {
        results.push({
          uri: "http://vocab.getty.edu/" + candidate.id,
          label: candidate.name,
          description: "score: " + candidate.score,
        });
      });
    } else {
      return Promise.reject("Getty: " + JSON.stringify(data));
    }
  } catch (error) {
    return Promise.reject("Getty: " + getErrorMessage(error));
  }
  return results;
}

async function gettyCONA(entity: string, numEntities: number) {
  const results: LinkMatch[] = [];
  const req_url = "http://vocabsservices.getty.edu/CONAService.asmx/CONAGetTermMatch";
  const req_params = `term=${entity}&logop=&notes=&facet=&wtype=&creator=&material=&location=&number=&geographic=&creation_start=&creation_end=&general_subject=&specific_subject=`;
  try {
    const response = await fetch(req_url + "?" + req_params, { method: "GET" });
    const xml = await response.text();
    // convert the xml response to json
    const data = await xml2js.parseStringPromise(xml);
    if (response.ok && data.Vocabulary.Count[0] > 0) {
      //@ts-expect-error no ts defs for api result
      data.Vocabulary.Subject.slice(0, numEntities).map((candidate) => {
        results.push({
          uri: "http://vocab.getty.edu/page/cona/" + candidate.Subject_ID[0],
          label: candidate.Preferred_Term[0]._,
          description: candidate.Cona_Label[0],
        });
      });
    } else {
      return Promise.reject("Getty: " + JSON.stringify(data));
    }
  } catch (error) {
    return Promise.reject("Getty: " + getErrorMessage(error));
  }
  return results;
}

async function gnd(entity: string, queryType: string, numEntities: number) {
  const results: LinkMatch[] = [];
  const req_url = `https://lobid.org/gnd`;
  const params = {
    headers: { Accept: "application/json" },
    method: "GET",
  };
  const searchParams = new URLSearchParams({
    q: entity,
    filter: "type:" + queryType,
    format: "json",
    size: numEntities.toString(),
  });
  try {
    const response = await fetch(req_url + `/search?${searchParams}`, params);
    const data = await response.json();
    if (response.ok) {
      //@ts-expect-error no ts defs for api result
      data.member.map((candidate) => {
        results.push({
          uri: candidate.id,
          label: candidate.preferredName,
          description: candidate.biographicalOrHistoricalInformation?.[0] ?? "",
        });
      });
    } else {
      return Promise.reject("GND: " + JSON.stringify(data));
    }
  } catch (error) {
    return Promise.reject("GND: " + getErrorMessage(error));
  }
  return results;
}

async function lincs(entity: string, typeFilter: string | string[] | undefined, numEntities: number) {
  const results: LinkMatch[] = [];
  const req_url = "https://authority.lincsproject.ca/reconcile/any";
  const req_body = { q1: { query: entity, type: typeFilter, limit: numEntities } };
  const searchParams = new URLSearchParams({ queries: JSON.stringify(req_body) });
  const params = {
    headers: { Accept: "application/json", "Content-Type": "application/x-www-form-urlencoded" },
    method: "POST",
    body: searchParams.toString(),
  };
  try {
    const response = await fetch(req_url, params);
    const data = await response.json();
    if (response.ok && !data.Error) {
      //@ts-expect-error no ts defs for api result
      data.q1.result.map((candidate) => {
        results.push({
          uri: candidate.id,
          label: candidate.name,
          description: candidate.type[0].id,
        });
      });
    } else {
      return Promise.reject("LINCS: " + JSON.stringify(data));
    }
  } catch (error) {
    return Promise.reject("LINCS: " + getErrorMessage(error));
  }
  return results;
}

async function viaf(entity: string, queryType: string, numEntities: number) {
  const results: LinkMatch[] = [];
  const req_url = `https://viaf.org/viaf/search?query=${queryType}="${entity}"&sortKeys=holdingscount&maximumRecords=${numEntities}&recordSchema=BriefVIAF`;
  const params = {
    headers: { Accept: "application/json" },
    method: "GET",
  };
  try {
    const response = await fetch(req_url, params);
    const data = await response.json();
    if (response.ok) {
      const records = data.searchRetrieveResponse.records.record;
      if (records) {
        //@ts-expect-error no ts defs for api result
        records.map((record) => {
          const candidate = record.recordData;
          const cluster = candidate["v:VIAFCluster"];
          const labelData = cluster["v:mainHeadings"]["v:data"];
          const label = Array.isArray(labelData) ? labelData[0]["v:text"] : labelData["v:text"];
          const description = Array.isArray(labelData) ? labelData[1]["v:text"] : undefined;
          results.push({
            uri: "http://viaf.org/viaf/" + cluster["v:viafID"],
            label: label,
            description: description,
          });
        });
      }
    } else {
      return Promise.reject("VIAF: " + JSON.stringify(data));
    }
  } catch (error) {
    return Promise.reject("VIAF: " + getErrorMessage(error));
  }
  return results;
}

async function wikidata(entity: string, numEntities: number) {
  const results: LinkMatch[] = [];
  const wdk = WBK({
    instance: "https://www.wikidata.org",
    sparqlEndpoint: "https://query.wikidata.org/sparql",
  });
  const req_url = wdk.searchEntities({ search: entity, language: "en", limit: numEntities });
  const params = {
    headers: { Accept: "application/json" },
    method: "GET",
  };
  try {
    const response = await fetch(req_url, params);
    const data = await response.json();
    if (response.ok && data.success == 1) {
      //@ts-expect-error no ts defs for api result
      data.search.map((candidate) => {
        results.push({
          uri: candidate.concepturi,
          label: candidate.label,
          description: candidate.description,
        });
      });
    } else {
      return Promise.reject("Wikidata: " + JSON.stringify(data));
    }
  } catch (error) {
    return Promise.reject("Wikidata: " + getErrorMessage(error));
  }
  return results;
}
