import { Entity, EntityResponse, SimpleEntity, FilteredEntityQuery } from "@lincs.project/lincs-api-contracts";
import { PAGE_SIZE, PREFIXES, filterByGraph, CONNECTION_COUNT_PRED, CACHE_GRAPH } from "@/helpers/sparql";

export function countUpdateSparql(resource: string, count: number): string {
  return `
    DELETE { GRAPH <${CACHE_GRAPH}> { 	
      <${resource}> <${CONNECTION_COUNT_PRED}> ?o
    } }
    WHERE { GRAPH <${CACHE_GRAPH}> {
      <${resource}> <${CONNECTION_COUNT_PRED}> ?o
    } }
    ;
    INSERT DATA { GRAPH <${CACHE_GRAPH}> { 	
      <${resource}> <${CONNECTION_COUNT_PRED}> ${count}
    } }  
  `
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createEntityResponse(data: any[]): EntityResponse {
  const results: EntityResponse = [];
  let currEntity: Entity = { resource: "" };
  data.map((row) => {
    if (row.entity.value != currEntity.resource) {
      if (currEntity.resource) results.push(currEntity);
      // init new entity
      currEntity = { resource: row.entity.value };
      if (row.enLabel) currEntity.resourceLabel = row.enLabel.value;
      if (row.entType) currEntity.type = row.entType.value;
      if (row.entLabel) currEntity.typeLabel = row.entLabel.value;
    }
    const record: SimpleEntity = {
      graph: row.g.value,
      resource: row.event.value,
    };
    if (row.evLabel) record.resourceLabel = row.evLabel.value;
    if (row.eveType) record.type = row.eveType.value;
    if (row.evtLabel) record.typeLabel = row.evtLabel.value;
    if (!currEntity.incoming) currEntity.incoming = [];
    currEntity.incoming.push(record);
  });
  if (currEntity.resource) results.push(currEntity);
  return results;
}

export function buildQuery(body: FilteredEntityQuery) {
  let query = PREFIXES;
  query += `
SELECT ?g ?event (MAX(?evl) AS ?evLabel) (SAMPLE(?evType) AS ?eveType) (MAX(?evtl) AS ?evtLabel)
    ?entity (MAX(?enl) AS ?enLabel) (SAMPLE(?enType) AS ?entType) (MAX(?tl) AS ?entLabel) WHERE {
  {
    SELECT DISTINCT ?g ?resource ?event ?entity WHERE {
      BIND(<${body.uri}> as ?resource)
      GRAPH ?g {
        {
          # All entities linked directly via event
          VALUES (?rp) {
            (crm:P7_took_place_at)
            (crm:P11_had_participant)
            (crm:P14_carried_out_by)
            (crm:P94_has_created)
            (crm:P98_brought_into_life)
            (crm:P100_was_death_of)
            (crm:P108_has_produced)
            (crm:P143_joined)
          }
          VALUES (?ep) {
            (crm:P7_took_place_at)
            (crm:P11_had_participant)
            (crm:P14_carried_out_by)
            (crm:P94_has_created)
            (crm:P96_by_mother)
            (crm:P97_from_father)
            (crm:P100_was_death_of)
            (crm:P108_has_produced)
            (crm:P143_joined)
          }
          ?event ?rp ?resource .
          ?event ?ep ?entity .
          FILTER(?entity != ?resource)
        }
        UNION
        {
          # linked entities via publisher AP
          ?event crm:P94_has_created ?entity .
          ?role crm:P01_has_domain ?event .
          ?role crm:P02_has_range ?resource .
        }
        UNION
        {
          # the other way around
          ?event crm:P94_has_created ?resource .
          ?role crm:P01_has_domain ?event .
          ?role crm:P02_has_range ?entity .
        }
        UNION
        {
          # Web Annotations is_about
          ?do crm:P129_is_about ?entity .
          ?source oa:hasSource ?do .
          ?event oa:hasTarget ?source ;
           	oa:hasBody ?resource .
        }
        VALUES (?types) {
          (crm:E12_Production)
          (crm:E21_Person)
          (crm:E22_Human-Made_Object)
          (crm:E39_Actor)
          (crm:E53_Place)
          (crm:E73_Information_Object)
          (crm:E74_Group)
          (frbroo:F1_Work)
          (frbroo:F2_Expression)
        }
        ?entity rdf:type ?types .
      }
      `;
      query += filterByGraph(body.graphs) + `
    }
    LIMIT ${PAGE_SIZE} OFFSET ${(body.page - 1) * PAGE_SIZE}
  }
  OPTIONAL {
    ?event rdfs:label ?evl
    FILTER (LANGMATCHES(LANG(?evl), "${body.language}"))
  }
  OPTIONAL {
    ?event rdfs:label ?evl
    FILTER (LANGMATCHES(LANG(?evl), ""))
  }
  OPTIONAL {
    ?event rdfs:label ?evl
    FILTER (LANGMATCHES(LANG(?evl), "en"))
  }
  OPTIONAL {
    ?event rdf:type ?evType .
    OPTIONAL {
      ?evType rdfs:label ?evtl .
      FILTER (LANGMATCHES(LANG(?evtl), "${body.language}"))
    }
    OPTIONAL {
      ?evType rdfs:label ?evtl .
      FILTER (LANGMATCHES(LANG(?evtl), ""))
    }
    OPTIONAL {
      ?evType rdfs:label ?evtl .
      FILTER (LANGMATCHES(LANG(?evtl), "en"))
    }
  }
  OPTIONAL {
    ?entity rdfs:label ?enl
    FILTER (LANGMATCHES(LANG(?enl), "${body.language}"))
  }
  OPTIONAL {
    ?entity rdfs:label ?enl
    FILTER (LANGMATCHES(LANG(?enl), ""))
  }
  OPTIONAL {
    ?entity rdfs:label ?enl
    FILTER (LANGMATCHES(LANG(?enl), "en"))
  }
  OPTIONAL {
    ?entity rdf:type ?enType .
    OPTIONAL {
      ?enType rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), "${body.language}"))
    }
    OPTIONAL {
      ?enType rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), ""))
    }
    OPTIONAL {
      ?enType rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), "en"))
    }
  }
}
GROUP BY ?g ?event ?entity
ORDER BY ?enLabel ?evLabel
  `;
  return query;
}
