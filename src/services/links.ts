import { EntityResponse, LinksQuery } from "@lincs.project/lincs-api-contracts";
import { PREFIXES, filterByGraph, filterPredicates } from "@/helpers/sparql";
import { expandEntities, formatOutgoing } from "./entity";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createEntityResponse(data: any[]): EntityResponse {
  const results = formatOutgoing(data);
  // respond with extracted labels and types
  return expandEntities(results);
}

export function buildOutgoing(body: LinksQuery) {
  let query = PREFIXES;
  query += `
SELECT ?g ?s ?p ?startDate ?endDate (MAX(?pl) AS ?pLabel) ?o (MAX(?ol) AS ?oLabel)
	(SAMPLE(?type) AS ?oType) (MAX(?tl) AS ?tLabel) WHERE {
  GRAPH ?g {
    bind (<${body.uri}> as ?entity1)
    bind (<${body.secondUri}> as ?entity2)
    # statements including entity1
    {
      ?s ?p1 ?entity1 .
    } UNION {
      ?entity1 ?p1 ?s .
    } UNION {
      # author to place. ?s is a statement
      ?role1 crm:P01_has_domain ?s ;
        crm:P02_has_range ?entity1 .
    } UNION {
      # author/publisher to instance of work. ?s is instance
      ?role1 crm:P02_has_range ?entity1 ;
        crm:P01_has_domain ?statement1 .
      ?statement1 crm:P94_has_created ?s .
    }
    # statements including entity2
    {
      ?s ?p2 ?entity2 .
    } UNION {
      ?entity2 ?p2 ?s .
    } UNION {
      # author to place. ?s is a statement
      ?role2 crm:P01_has_domain ?s ;
        crm:P02_has_range ?entity2 .
    } UNION {
      # author/publisher to instance of work
      ?role2 crm:P02_has_range ?entity2 ;
        crm:P01_has_domain ?statement2 .
      ?statement2 crm:P94_has_created ?s .
    }
    # rest of statements with same subject
    ?s ?p ?o .
  }
  OPTIONAL {
    ?p rdfs:label ?pl .
    FILTER(LANGMATCHES(LANG(?pl), "${body.language}"))
  }
  OPTIONAL {
    ?p rdfs:label ?pl .
    FILTER(LANGMATCHES(LANG(?pl), ""))
  }
  OPTIONAL {
    ?p rdfs:label ?pl .
    FILTER(LANGMATCHES(LANG(?pl), "en"))
  }
  OPTIONAL {
    ?o rdfs:label ?ol .
    FILTER(LANGMATCHES(LANG(?ol), "${body.language}"))
  }
  OPTIONAL {
    ?o rdfs:label ?ol .
    FILTER(LANGMATCHES(LANG(?ol), ""))
  }
  OPTIONAL {
    ?o rdfs:label ?ol .
    FILTER(LANGMATCHES(LANG(?ol), "en"))
  }
  OPTIONAL {
    ?o rdf:type ?type .
    OPTIONAL {
      ?type rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), "${body.language}"))
    }
    OPTIONAL {
      ?type rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), ""))
    }
    OPTIONAL {
      ?type rdfs:label ?tl .
      FILTER (LANGMATCHES(LANG(?tl), "en"))
    }
  }
  OPTIONAL {
    ?s crm:P4_has_time-span ?ts.
    ?ts crm:P82a_begin_of_the_begin ?startDate;
        crm:P82b_end_of_the_end ?endDate.
  }
  `;
  query += filterPredicates(body.predicateFilter) + `
  `;
  query += filterByGraph(body.graphs);
  if (body.language === "en") {
    query += `
    FILTER (!isLiteral(?o) || LANGMATCHES(LANG(?o), "${body.language}") || LANGMATCHES(LANG(?o), ""))`
  }
  query += `
}
GROUP BY ?g ?s ?p ?o ?startDate ?endDate
ORDER BY ?s ?pLabel ?oLabel
  `;
  return query;
}
