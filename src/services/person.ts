import { EntityStatement, EntityStatements, Person, Resource, SimpleEntityQuery } from "@lincs.project/lincs-api-contracts";
import { PREFIXES } from "@/helpers/sparql";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createPerson(data: any[]): Person[] {
  const results: Person[] = [];
  let currPerson: Person = { uri: "" };
  data.map((row) => {
    if (row.entity.value != currPerson.uri) {
      if (currPerson.uri) {
        currPerson.pursuits?.sort((a, b) => ((a.uri ?? "") > (b.uri ?? "") ? 1 : -1));
        currPerson.attributes?.sort((a, b) => ((a.uri ?? "") > (b.uri ?? "") ? 1 : -1));
        currPerson.works?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
        results.push(currPerson);
      }
      // init new person
      currPerson = { uri: row.entity.value };
    }
    const resource: Resource = {
      uri: row.resource?.value,
      label: row.value?.value,
    };
    if ("birthDate" === row.type.value) currPerson.birthDate = resource.label;
    if ("deathDate" === row.type.value) currPerson.deathDate = resource.label;
    if ("birthPlace" === row.type.value) currPerson.birthPlace = resource.label;
    if ("deathPlace" === row.type.value) currPerson.deathPlace = resource.label;
    if ("label" === row.type.value) {
      // Write new label if current label is empty or if the new label comes after the current one alphabetically
      if (!currPerson.label || (resource.label && resource.label > currPerson.label)) {
        currPerson.graph = row.g?.value;
        currPerson.label = resource.label;
      }
    }
    if ("prefLabel" === row.type.value && !currPerson.prefLabel) currPerson.prefLabel = resource.label;
    if ("note" === row.type.value && !currPerson.note) currPerson.note = resource.label;
    if ("image" === row.type.value && !currPerson.image) currPerson.image = resource.uri;
    if ("sameAs" === row.type.value && resource.uri) {
      if (!currPerson.sameAs) currPerson.sameAs = [];
      const index = currPerson.sameAs.findIndex(element => element === resource.uri);
      if (index === -1) {
        currPerson.sameAs.push(resource.uri);
      } else {
        currPerson.sameAs[index] = resource.uri;
      }
    }
    if ("pursuit" === row.type.value) {
      if (!currPerson.pursuits) currPerson.pursuits = [];
      currPerson.pursuits.push(resource);
    }
    if ("attribute" === row.type.value) {
      if (!currPerson.attributes) currPerson.attributes = [];
      currPerson.attributes.push(resource);
    }
    if ("group" === row.type.value) {
      if (!currPerson.groups) currPerson.groups = [];
      currPerson.groups.push(resource);
    }
    if ("work" === row.type.value) {
      if (!currPerson.works) currPerson.works = [];
      const index = currPerson.works.findIndex(element => element.uri === row.resource.value);
      if (index === -1) {
        currPerson.works.push(resource);
      } else {
        currPerson.works[index] = resource;
      }
    }
  });

  if (currPerson.uri) {
    currPerson.pursuits?.sort((a, b) => ((a.uri ?? "") > (b.uri ?? "") ? 1 : -1));
    currPerson.attributes?.sort((a, b) => ((a.uri ?? "") > (b.uri ?? "") ? 1 : -1));
    currPerson.groups?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
    currPerson.works?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
    results.push(currPerson);
  }
  return results;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createStatements(data: any[]): EntityStatements[] {
  const results: EntityStatements[] = [];
  let currPerson: EntityStatements = { uri: "", statements: [] };
  data.map((row) => {
    if (row.entity.value != currPerson.uri) {
      if (currPerson.uri) {
        currPerson.statements?.sort((a, b) => ((a.type ?? "") > (b.type ?? "") ? 1 : -1));
        results.push(currPerson);
      }
      // init new person
      currPerson = { uri: row.entity.value, statements: [] };
    }
    const statement: EntityStatement = {
      type: row.type?.value,
      graph: row.g?.value,
      uri: row.resource?.value,
      value: row.value?.value,
    };
    currPerson.statements.push(statement);
  });
  if (currPerson.uri) {
    currPerson.statements?.sort((a, b) => ((a.type ?? "") > (b.type ?? "") ? 1 : -1));
    results.push(currPerson);
  }
  return results;
}

export function buildQuery(body: SimpleEntityQuery) {
  let query = PREFIXES;
  query += `
SELECT DISTINCT ?entity ?type ?resource (COALESCE(SAMPLE(?pval), ?val) as ?value) ?g WHERE {
  VALUES (?entity) { `;
  for (const uri of body.uris) {
    query += `(<${uri}>) `;
  }
  query += `
  }
  GRAPH ?g {
    {
      {
        BIND('birthDate' as ?type)
        ?resource crm:P98_brought_into_life ?entity .
      } UNION {
        BIND('deathDate' as ?type)
        ?resource crm:P100_was_death_of ?entity . 
      }
      ?resource crm:P4_has_time-span/crm:P82a_begin_of_the_begin ?date .
      BIND(CONCAT(CONCAT(CONCAT(CONCAT(str(YEAR(?date)), "-"), str(MONTH(?date))), "-"), str(DAY(?date))) as ?val)
    } UNION {
      {
        BIND('birthPlace' as ?type)
        {
          ?date crm:P98_brought_into_life ?entity .
          ?date crm:P7_took_place_at/crm:P89_falls_within ?resource .
        } UNION {
          ?date crm:P98_brought_into_life ?entity .
          ?date crm:P7_took_place_at ?resource .
        }
      } UNION {
        BIND('deathPlace' as ?type)
        {
          ?date crm:P100_was_death_of ?entity .
          ?date crm:P7_took_place_at/crm:P89_falls_within ?resource .
        } UNION {
          ?date crm:P100_was_death_of ?entity .
          ?date crm:P7_took_place_at ?resource .
        }
      }
      OPTIONAL {
        ?resource rdfs:label ?val
        FILTER (LANGMATCHES(LANG(?val), "${body.language}"))
      }
      OPTIONAL {
        ?resource rdfs:label ?val
        FILTER (LANGMATCHES(LANG(?val), ""))
      }
      OPTIONAL {
        ?resource rdfs:label ?val
        FILTER (LANGMATCHES(LANG(?val), "en"))
      }
    } UNION {
      {
        BIND('label' as ?type)
        ?entity rdfs:label ?temp .
        OPTIONAL {
          ?entity rdfs:label ?val
          FILTER (LANGMATCHES(LANG(?val), "${body.language}"))
        }
        OPTIONAL {
          ?entity rdfs:label ?val
          FILTER (LANGMATCHES(LANG(?val), ""))
        }
        OPTIONAL {
          ?entity rdfs:label ?val
          FILTER (LANGMATCHES(LANG(?val), "en"))
        }
      } UNION {
        BIND('prefLabel' as ?type)
        ?entity skos:prefLabel ?val
        FILTER (LANGMATCHES(LANG(?val), "${body.language}") || (LANGMATCHES(LANG(?val), "")))
      } UNION {
        BIND('image' as ?type)
        ?resource crm:P138_represents ?entity
      } UNION {
        BIND('sameAs' as ?type)
        ?entity owl:sameAs ?resource
      } UNION {
        BIND('note' as ?type)
        ?entity crm:P3_has_note ?val
      }
    } UNION {
      BIND('pursuit' as ?type)
      ?resource crm:P14_carried_out_by ?entity .
      {
        ?resource crm:P2_has_type <http://id.lincsproject.ca/occupation/Occupation> .
      } UNION {
        ?resource crm:P2_has_type <http://id.lincsproject.ca/occupation/OccupationEvent> .
      } UNION {
        ?resource crm:P2_has_type <http://id.lincsproject.ca/event/OccupationEvent> .
      }
      ?resource crm:P2_has_type ?role .
      FILTER(?role not in (
          <http://id.lincsproject.ca/occupation/OccupationEvent>,
          <http://id.lincsproject.ca/occupation/paidOccupation>,
          <http://id.lincsproject.ca/occupation/volunteerOccupation>,
          <http://id.lincsproject.ca/occupation/familyBasedOccupation>,
          <http://id.lincsproject.ca/event/OccupationEvent>,
          <http://id.lincsproject.ca/event/paidOccupation>,
          <http://id.lincsproject.ca/event/volunteerOccupation>,
          <http://id.lincsproject.ca/event/familyBasedOccupation>
        ))
      OPTIONAL {
        ?role skos:prefLabel ?pval
        FILTER (LANGMATCHES(LANG(?pval), "${body.language}"))
      }
      OPTIONAL {
        ?role skos:prefLabel ?pval
        FILTER (LANGMATCHES(LANG(?pval), ""))
      }
      OPTIONAL {
        ?role skos:prefLabel ?pval
        FILTER (LANGMATCHES(LANG(?pval), "en"))
      }
      OPTIONAL {
        ?role rdfs:label ?pval
        FILTER (LANGMATCHES(LANG(?pval), "${body.language}"))
      }
      OPTIONAL {
        ?role rdfs:label ?pval
        FILTER (LANGMATCHES(LANG(?pval), ""))
      }
      OPTIONAL {
        ?role rdfs:label ?pval
        FILTER (LANGMATCHES(LANG(?pval), "en"))
      }
      FILTER(STR(?pval) != "occupation" && STRLEN(?pval) != 0)
    } UNION {
      BIND('attribute' as ?type)
      ?resource crm:P140_assigned_attribute_to ?entity ;
                crm:P2_has_type ?context ;
                crm:P141_assigned ?assigned .
      ?assigned crm:P16_used_specific_object ?object .
      OPTIONAL {
        ?object rdfs:label ?o_label
        FILTER (LANGMATCHES(LANG(?o_label), "${body.language}"))
    }
      OPTIONAL {
        ?object rdfs:label ?o_label
        FILTER (LANGMATCHES(LANG(?o_label), ""))
    }
      OPTIONAL {
        ?object rdfs:label ?o_label
        FILTER (LANGMATCHES(LANG(?o_label), "en"))
    }
      OPTIONAL {
        ?context rdfs:label ?c_label
        FILTER (LANGMATCHES(LANG(?c_label), "${body.language}"))
      }
      OPTIONAL {
        ?context rdfs:label ?c_label
        FILTER (LANGMATCHES(LANG(?c_label), ""))
      }
      OPTIONAL {
        ?context rdfs:label ?c_label
        FILTER (LANGMATCHES(LANG(?c_label), "en"))
      }
      BIND(CONCAT(CONCAT(?c_label, ": "), ?o_label) as ?val)
      FILTER(STRLEN(?val) != 0)
    } UNION {
      BIND('group' as ?type)
      {
        ?resource crm:P107_has_current_or_former_member ?entity .
      }
      OPTIONAL {
        ?resource rdfs:label ?val
        FILTER (LANGMATCHES(LANG(?val), "${body.language}"))
      }
      OPTIONAL {
        ?resource rdfs:label ?val
        FILTER (LANGMATCHES(LANG(?val), ""))
      }
      OPTIONAL {
        ?resource rdfs:label ?val
        FILTER (LANGMATCHES(LANG(?val), "en"))
      }
    } UNION {
      BIND('work' as ?type)
      {
        ?range crm:P02_has_range ?entity .
        ?range crm:P01_has_domain ?event .
        ?event crm:P94_has_created ?work .
      }
      BIND(?work as ?resource)
      OPTIONAL {
        ?work rdfs:label ?val
        FILTER (LANGMATCHES(LANG(?val), "${body.language}"))
      }
      OPTIONAL {
        ?work rdfs:label ?val
        FILTER (LANGMATCHES(LANG(?val), ""))
      }
      OPTIONAL {
        ?work rdfs:label ?val
        FILTER (LANGMATCHES(LANG(?val), "en"))
      }
    }
  }
}
GROUP BY ?entity ?type ?resource ?g ?val
ORDER BY ?entity ?type ?resource
  `;
  return query;
}
