import { Place, Resource, SimpleEntityQuery } from "@lincs.project/lincs-api-contracts";
import { getErrorMessage } from "@/helpers/error";
import { PREFIXES } from "@/helpers/sparql";
import { GEONAMES_ENDPOINT } from "@/globals";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createPlace(data: any[]): Place[] {
  const results: Place[] = [];
  let currPlace: Place = { uri: "" };
  data.map((row) => {
    if (row.entity.value != currPlace.uri) {
      if (currPlace.uri) {
        currPlace.labels?.sort((a, b) => ((a.uri ?? "") > (b.uri ?? "") ? 1 : -1));
        currPlace.bornHere?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
        currPlace.diedHere?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
        results.push(currPlace);
      }
      // init new place
      currPlace = { uri: row.entity.value };
    }
    const resource: Resource = {
      uri: row.resource?.value,
      label: row.value?.value,
    };
    if ("label" === row.type.value) {
      // Write new label if current label is empty or if the new label comes after the current one alphabetically
      if (!currPlace.label || (resource.label && resource.label > currPlace.label)) {
        currPlace.graph = row.g?.value;
        currPlace.label = resource.label;
      }
    }
    if ("labels" === row.type.value) {
      if (!currPlace.labels) currPlace.labels = [];
      currPlace.labels.push(resource);
    }
    if ("definedBy" === row.type.value) currPlace.definedBy = resource.label;
    if ("type" === row.type.value) {
      if (!currPlace.types) currPlace.types = [];
      const index = currPlace.types.findIndex(element => element.uri === resource.label);
      if (index === -1) {
        currPlace.types.push(resource);
      } else {
        currPlace.types[index] = resource;
      }
    }
    if ("bornHere" === row.type.value) {
      if (!currPlace.bornHere) currPlace.bornHere = [];
      const index = currPlace.bornHere.findIndex(element => element.uri === resource.label);
      if (index === -1) {
        currPlace.bornHere.push(resource);
      } else {
        currPlace.bornHere[index] = resource;
      }
    }
    if ("diedHere" === row.type.value) {
      if (!currPlace.diedHere) currPlace.diedHere = [];
      const index = currPlace.diedHere.findIndex(element => element.uri === resource.label);
      if (index === -1) {
        currPlace.diedHere.push(resource);
      } else {
        currPlace.diedHere[index] = resource;
      }
    }
  });

  if (currPlace.uri) {
    currPlace.labels?.sort((a, b) => ((a.uri ?? "") > (b.uri ?? "") ? 1 : -1));
    currPlace.bornHere?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
    currPlace.diedHere?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
    results.push(currPlace);
  }
  return results;
}

export async function lookupGeoClass(geoNamesId: string): Promise<string> {
  if (!geoNamesId) {
    return Promise.reject("Cannot link to empty entity");
  }
  const req_url = `${GEONAMES_ENDPOINT}?id=${geoNamesId}`;
  const params = {
    headers: { Accept: "application/json" },
    method: "GET",
  };
  try {
    const response = await fetch(req_url, params);
    const data = await response.json();
    if (response.ok && !data.Error) {
      return data.data[0].properties.featureClass;
    } else {
      return Promise.reject("lookupGeoClass: " + JSON.stringify(data));
    }
  } catch (error) {
    return Promise.reject("lookupGeoClass: " + getErrorMessage(error));
  }
}

export function buildQuery(body: SimpleEntityQuery) {
  let query = PREFIXES;
  query += `
SELECT DISTINCT ?entity ?type ?resource ?value ?g WHERE {
  VALUES (?entity) { `;
  for (const uri of body.uris) {
    query += `(<${uri}>) `;
  }
  query += `
  }
  GRAPH ?g {
    {
      BIND('label' as ?type)
      ?entity rdfs:label ?temp .
      OPTIONAL {
        ?entity rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
        ?entity rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
        ?entity rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    } UNION {
      BIND('labels' as ?type)
      BIND(skos:altLabel as ?resource)
      ?entity skos:altLabel ?value .
      FILTER (LANGMATCHES(LANG(?value), "${body.language}") || (LANGMATCHES(LANG(?value), "")))
    } UNION {
      BIND('labels' as ?type)
      BIND(skos:hiddenLabel as ?resource)
      ?entity skos:hiddenLabel ?value .
      FILTER (LANGMATCHES(LANG(?value), "${body.language}") || (LANGMATCHES(LANG(?value), "")))
    } UNION {
      BIND('labels' as ?type)
      BIND(skos:prefLabel as ?resource)
      ?entity skos:prefLabel ?value .
      FILTER (LANGMATCHES(LANG(?value), "${body.language}") || (LANGMATCHES(LANG(?value), "")))
    } UNION {
      BIND('definedBy' as ?type)
      ?entity crm:P168_place_is_defined_by ?value .
    } UNION {
      BIND('type' as ?type)
      ?entity crm:P2_has_type ?resource .
      OPTIONAL {
        ?resource rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
        ?resource rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
        ?resource rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    } UNION {
      BIND('bornHere' as ?type)
      {
        ?date crm:P98_brought_into_life ?resource .
        ?date crm:P7_took_place_at/crm:P89_falls_within ?entity .
      } UNION {
        ?date crm:P98_brought_into_life ?resource .
        ?date crm:P7_took_place_at ?entity .
      }
      OPTIONAL {
        ?resource rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
        ?resource rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
        ?resource rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    } UNION {
      BIND('diedHere' as ?type)
      {
        ?date crm:P100_was_death_of ?resource .
        ?date crm:P7_took_place_at/crm:P89_falls_within ?entity .
      } UNION {
        ?date crm:P100_was_death_of ?resource .
        ?date crm:P7_took_place_at ?entity .
      }
      OPTIONAL {
        ?resource rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
        ?resource rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
        ?resource rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    }
  }
}
  `;
  return query;
}
