import { Resource, SimpleEntityQuery, Work } from "@lincs.project/lincs-api-contracts";
import { PREFIXES } from "@/helpers/sparql";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createWork(data: any[]): Work[] {
  const results: Work[] = [];
  let currWork: Work = { uri: "" };
  data.map((row) => {
    if (row.entity.value != currWork.uri) {
      if (currWork.uri) {
        currWork.types?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
        currWork.creators?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
        results.push(currWork);
      }
      // init new work
      currWork = { uri: row.entity.value };
    }
    const resource: Resource = {
      uri: row.resource?.value,
      label: row.value?.value,
    };
    if ("label" === row.type.value) {
      // Write new label if current label is empty or if the new label comes after the current one alphabetically
      if (!currWork.label || (resource.label && resource.label > currWork.label)) {
        currWork.graph = row.g?.value;
        currWork.label = resource.label;
      }
    }
    if ("date" === row.type.value) currWork.createDate = resource.label;
    if ("type" === row.type.value) {
      if (!currWork.types) currWork.types = [];
      const index = currWork.types.findIndex(element => element.uri === row.resource.value);
      if (index === -1) {
        currWork.types.push(resource);
      } else {
        currWork.types[index] = resource;
      }
    }
    if ("creator" === row.type.value) {
      if (!currWork.creators) currWork.creators = [];
      const index = currWork.creators.findIndex(element => element.uri === row.resource.value);
      if (index === -1) {
        currWork.creators.push(resource);
      } else {
        currWork.creators[index] = resource;
      }
    }
  });

  if (currWork.uri) {
    currWork.types?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
    currWork.creators?.sort((a, b) => ((a.label ?? "") > (b.label ?? "") ? 1 : -1));
    results.push(currWork);
  }
  return results;
}

export function buildQuery(body: SimpleEntityQuery) {
  let query = PREFIXES;
  query += `
SELECT ?entity ?type ?resource ?value ?g WHERE {
  VALUES (?entity) { `;
  for (const uri of body.uris) {
    query += `(<${uri}>) `;
  }
  query += `
  }
  GRAPH ?g {
    {
      BIND('label' as ?type)
      ?entity rdfs:label ?temp .
      OPTIONAL {
        ?entity rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
        ?entity rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
        ?entity rdfs:label ?value .
        FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    } UNION {
      BIND('type' as ?type)
      ?entity crm:P2_has_type ?resource .
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    } UNION {
      BIND('date' as ?type)
      ?resource crm:P94_has_created|crm:P108_has_produced ?entity .
      ?resource crm:P4_has_time-span ?timespan .
      ?timespan crm:P82a_begin_of_the_begin ?date .
      BIND(CONCAT(CONCAT(CONCAT(CONCAT(str(YEAR(?date)), "-"), str(MONTH(?date))), "-"), str(DAY(?date))) as ?value)
    } UNION {
      BIND('creator' as ?type)
      {
        ?event crm:P94_has_created|crm:P108_has_produced ?entity .
        ?event crm:P14_carried_out_by ?resource
      } UNION {
        ?event crm:P94_has_created|crm:P108_has_produced ?entity .
        ?role crm:P01_has_domain ?event .
        ?role crm:P02_has_range ?resource .
      }
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), "${body.language}"))
      }
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), ""))
      }
      OPTIONAL {
        ?resource rdfs:label ?value
        FILTER (LANGMATCHES(LANG(?value), "en"))
      }
    }
  }
}
  `;
  return query;
}
