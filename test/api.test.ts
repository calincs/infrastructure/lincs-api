import { describe, expect, test } from '@jest/globals';
import request from "supertest";
import { getApplication } from "@/server";

const req = request(getApplication());

describe("Health endpoint", () => {
  test("healthz", async () => {
    const res = await req.get("/healthz");
    expect(res.statusCode).toEqual(200);
    expect(res.text).toEqual("OK");
  });
});
